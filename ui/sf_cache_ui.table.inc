<?php

/**
 * @file
 * Contains functions for rendering tables.
 */

/**
 * Generic table generation function for Support File Cache module.
 */
function theme_sf_cache_ui_table($element) {
  // Set default settings for the table.
  $element += array('#table_type' => 'file');

  // Create the headers.
  $header = array();
  foreach ($element['#columns'] as $cid => $title) {
    $cell = array('class' => 'sf-cache-'. $cid);

    if ($title === TRUE) {
      switch ($cid) {
        case 'file_indicator':  $cell['data'] = '';           break;
        case 'file_checked':    $cell['data'] = '';           break;
        case 'file_enabled':    $cell['data'] = '✔';
                                $cell['title'] = t('Include this file'); break;
        case 'bundle_enabled':  $cell['data'] = '✔';
                                $cell['title'] = t('Include this bundle'); break;
        case 'minify':          $cell['data'] = '▼';
                                $cell['title'] = t('Minify'); break;
        case 'file_name':       $cell['data'] = t('Path');    break;
        case 'bundle_selector': $cell['data'] = t('Bundle');  break;
        case 'weight_selector': $cell['data'] = t('Weight');  break;
        case 'current_bundle':  $cell['data'] = t('Current Bundle'); break;
        case 'file_size':       $cell['data'] = t('Size');    break;
        case 'file_options':
        case 'bundle_options':  $cell['data'] = t('Options'); break;
        case 'bundle_title':    $cell['data'] = t('Title');   break;
      }
    }

    $header[] = $cell;
  }

  // Create the rows based on the headers.
  $rows = array();
  foreach (element_children($element) as $id) {
    $row = array();
    foreach ($element['#columns'] as $cid => $title) {
      // Insert content into the cell depending on the associated header column.
      $function = 'sf_cache_ui_table_cell_'. $cid;
      if (function_exists($function)) {
        $row[] = $function($element[$id], ($element['#table_type'] == 'bundle' ? $element[$id]['#bundle'] : $element[$id]['#file']));
      }
      else {
        $row[] = '';
      }
    }

    $rows[] = $row;
  }

  switch ($element['#table_type']) {
    case 'file-by-bundle':
    case 'file-by-bundle-read-only':
      if ($element['#bundle']['bid'] != SF_CACHE_UNBUNDLED) {
        $rows[] = array(
          'data' => array(
            array(
              'colspan' => count($element['#columns']) - 2,
              'data' => t('Total'),
              'class' => 'sf-cache-total',
            ),
            sf_cache_ui_table_cell_file_size(NULL, $element['#bundle']),
            ($element['#table_type'] != 'file-by-bundle-read-only') ? l(t('edit bundle'), 'admin/build/sf_cache/bundle/'. $element['#bundle']['bid'] .'/edit') : '',
          ),
          'class' => 'sf-cache-total-row',
        );
      }
  }

  // Determine the behavior when no rows are present.
  if (empty($rows)) {
    if (isset($element['#empty'])) {
      // Either create a blank row indicating that there is no data.
      $rows[] = array('', array('data' => $element['#empty'], 'colspan' => count($header)));
    }
    else {
      // ...or don't return a table when the empty string is not set.
      return '';
    }
  }

  $output = '';
  $caption = isset($element['#caption']) ? $element['#caption'] : NULL;

  if (isset($element['#title'])) {
    $output .= '<h3>'. check_plain($element['#title']) .'</h3>';
  }

  $output .= theme('table', $header, $rows, array('class' => 'sf-cache-table'), $caption);

  return $output;
}

/**
 * Table cell functions.
 */

function sf_cache_ui_table_cell_file_checked($element, $obj) {
  // A checkbox indicating whether the file is selected.
  return drupal_render($element['checked']);
}

function sf_cache_ui_table_cell_file_indicator($element, $obj) {
  // An indicator which is visible when the file does not exist.
  return (empty($obj['hash'])) ? array('class' => 'sf-cache-file-missing', 'title' => t('This file is missing.')) : array();
}

function sf_cache_ui_table_cell_file_enabled($element, $obj) {
  if (empty($obj['hash'])) {
    // The file does not exist and therefore can't be included anyway.
    return array('class' => 'sf-cache-file-missing', 'title' => t('This file is missing.'));
  }
  else {
    // A checkbox for selecting whether the file is enabled or not.
    return drupal_render($element['enabled']);
  }
}

function sf_cache_ui_table_cell_bundle_enabled($element, $obj) {
  // A checkbox for selecting whether the bundle is enabled or not.
  return drupal_render($element['enabled']);
}

function sf_cache_ui_table_cell_minify($element, $obj) {
  // A checkbox indicating whether the file should be minified.
  return drupal_render($element['minify']);
}

function sf_cache_ui_table_cell_file_name($element, $obj) {
  if (!empty($obj['hash'])) {
    $link = t('<small>(<a href="@source">source</a>)</small>', array('@source' => sf_cache_url(sf_cache_file_path($obj['filepath'], $obj['kind']))));
  }
  else {
    $link = '';
  }

  $title = check_plain($obj['filepath']);
  if (!empty($obj['hash'])) {
    $title = '<label for="'. $element['enabled']['#id'] .'">'. $title .'</label>';
  }

  // The filename.
  return $title .' '. $link;
}

function sf_cache_ui_table_cell_bundle_selector($element, $obj) {
  // A dropdown box with all possible bundles.
  return drupal_render($element['bid']);
}

function sf_cache_ui_table_cell_weight_selector($element, $obj) {
  // A dropdown weight selection box.
  return drupal_render($element['weight']);
}

function sf_cache_ui_table_cell_current_bundle($element, $obj) {
  // The title of the current bundle.
  if ($obj['bid'] != SF_CACHE_UNBUNDLED) {
    return l($element['#bundle_title'], 'admin/build/sf_cache/bundle/'. $obj['bid'] .'/edit', array('query' => drupal_get_destination()));
  }
  else {
    return check_plain($element['#bundle_title']);
  }
}

function sf_cache_ui_table_cell_file_size($element, $obj) {
  static $minification_modes = NULL;
  // Fetch the minification modes.
  if ($minification_modes === NULL) {
    $minification_modes = array(
      'css' => variable_get('sf_cache_minify_css_mode', SF_CACHE_MINIFICATION_BUILTIN),
      'js' => variable_get('sf_cache_minify_js_mode', SF_CACHE_MINIFICATION_DISABLED),
    );
  }

  $use_processed = (
    (!isset($obj['hash']) || !empty($obj['hash'])) &&
    $minification_modes[$obj['kind']] != SF_CACHE_MINIFICATION_DISABLED &&
    $obj['minify'] &&
    !empty($obj['processed']) &&
    $obj['size'] != $obj['processed_size']
  );

  if ($use_processed) {
    $display_size = $obj['processed_size'];
    $data = '▼ '. format_size($display_size);
    $title = t('Original size: @size; Savings: @savings%', array(
      '@size' => format_size($obj['size']),
      '@savings' => ($obj['size'] ? round(100 - $obj['processed_size'] / $obj['size'] * 100) : 0),
    ));
  }
  else {
    $display_size = $obj['size'];
    $data = format_size($display_size);
    $title = t('(Regular size)');
  }

  // The file size with color indication.
  return array(
    'data' => str_replace(' ', '&nbsp;', $data),
    'class' => 'sf-cache-sv-'. _sf_cache_ui_size_to_color($display_size),
    'title' => $title,
  );
}

function sf_cache_ui_table_cell_file_options($element, $obj) {
  // Options associated with the file.
  return l(t('edit'), 'admin/build/sf_cache/file/'. $obj['fid'] .'/edit', array('query' => drupal_get_destination(), 'html' => TRUE)) ." &nbsp;&nbsp; ".
    l(t('delete'), 'admin/build/sf_cache/file/'. $obj['fid'] .'/delete', array('query' => drupal_get_destination(), 'html' => TRUE));
}

function sf_cache_ui_table_cell_bundle_title($element, $obj) {
  return '<label for="edit-'. $obj['kind'] .'-'. $obj['bid'] .'-enabled">'. check_plain($obj['title']) .'</label>';
}

function sf_cache_ui_table_cell_bundle_options($element, $obj) {
  return l(t('edit'), 'admin/build/sf_cache/bundle/'. $obj['bid'] .'/edit', array('query' => drupal_get_destination(), 'html' => TRUE)) ." &nbsp;&nbsp; ".
    l(t('delete'), 'admin/build/sf_cache/bundle/'. $obj['bid'] .'/delete', array('query' => drupal_get_destination(), 'html' => TRUE));
}

/**
 * Returns a color code matching the file size.
 *
 * @param $size
 *   The file size in bytes.
 * @return
 *   A code from 1 to 6 depending on the file size.
 */
function _sf_cache_ui_size_to_color($size) {
      if ($size <= 512)   return 1;
  elseif ($size <= 2048)  return 2;
  elseif ($size <= 8192)  return 3;
  elseif ($size <= 16384) return 4;
  elseif ($size <= 49152) return 5;
  else                    return 6;
}
