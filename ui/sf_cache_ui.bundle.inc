<?php

/**
 * @file
 * Provides the interface for managing the bundle entities.
 */

/**
 * Menu callback; displays the bundle list page.
 */
function sf_cache_ui_bundle_list(&$form_values) {
  // Create a raw form structure; to be filled later.
  $form = array(
    'css' => array(
      '#tree' => TRUE,
      '#theme' => 'sf_cache_ui_bundle_list_table',
      '#title' => t('CSS bundles')
    ),
    'js' => array(
      '#tree' => TRUE,
      '#theme' => 'sf_cache_ui_bundle_list_table',
      '#title' => t('JavaScript bundles')
    ),
  );

  $bundle_pool = sf_cache_bundle_load();

  foreach ($bundle_pool as $kind => $bundles) {
    foreach ($bundles as $bid => $bundle) {
      // Add the bundle to the form.
      $form[$kind][$bid] = array(
        '#tree' => TRUE,
        '#bundle' => $bundle,
        'enabled' => array(
          '#type' => 'checkbox',
          '#default_value' => $bundle['enabled'],
          '#attributes' => array(
            'title' => t('When checked, files in this bundle will be replaced by this bundle.'),
          ),
        ),
        'minify' => array(
          '#type' => 'checkbox',
          '#default_value' => $bundle['minify'],
          '#attributes' => array(
            'title' => t('Check to minify this bundle.'),
          ),
        ),
      );
    }
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  return $form;
}

/**
 * Submit callback; saves the status of the bundles.
 */
function sf_cache_ui_bundle_list_submit($form, &$form_state) {
  $bundle_pool = sf_cache_bundle_load();

  foreach ($bundle_pool as $kind => $bundles) {
    foreach ($bundles as $bid => $bundle) {
      if (isset($form_state['values'][$kind][$bid])) {
        sf_cache_bundle_save(array(
          'bid' => $bid,
          'enabled' => (bool)$form_state['values'][$kind][$bid]['enabled'],
          'minify' => (bool)$form_state['values'][$kind][$bid]['minify'],
        ), FALSE);
      }
    }
  }

  // Update all files accordingly.
  sf_cache_ui_submit_update();
}

/**
 * Themes a bundle list table.
 */
function theme_sf_cache_ui_bundle_list_table($element) {
  $element['#columns'] = array(
    'bundle_enabled' => TRUE,
    'bundle_title' => TRUE,
    'minify' => TRUE,
    'file_size' => TRUE,
    'bundle_options' => TRUE,
  );

  $element['#table_type'] = 'bundle';
  $element['#empty'] = t('No bundles available.');

  // Include the table functions and render the table.
  module_load_include('table.inc', 'sf_cache_ui', 'sf_cache_ui');
  return theme('sf_cache_ui_table', $element);
}

/**
 * Menu callback; displays a form for adding or editing a bundle.
 *
 * @param $bid
 *   (Optional) The bundle ID of the bundle that is being edited.
 */
function sf_cache_ui_bundle_edit(&$form_values, $bundle = NULL) {
  $form = array();

  if (!isset($bundle)) {
    $bundle = array(
      'title' => '',
      'minify' => TRUE,
      'cc' => '',
    );
  }

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('The title of the bundle.'),
    '#size' => 40,
    '#maxlength' => 255,
    '#default_value' => $bundle['title'],
    '#required' => TRUE,
  );

  $form['kind'] = array(
    '#type' => 'radios',
    '#title' => t('Type'),
    '#description' => t('Select the type of the bundle.'),
    '#options' => array(
      'css' => t('CSS'),
      'js' => t('JavaScript')
    ),
    '#required' => TRUE,
  );

  $form['minify'] = array(
    '#type' => 'checkbox',
    '#title' => t('Minify'),
    '#description' => t('Defines whether the bundle should be minified using the <a href="@url">selected minification method</a>.', array('@url' => url('admin/build/sf_cache/config/minify'))),
    '#default_value' => $bundle['minify'],
  );

  $form['cc'] = array(
    '#type' => 'textfield',
    '#title' => t('Conditional Comments'),
    '#description' => t('When filled in, the bundle will be loaded only in clients which interpret conditional comments and which are matched by this rule. Example: <kbd>"lt IE 7"</kbd>'),
    '#size' => 40,
    '#default_value' => $bundle['cc'],
  );

  $form['buttons'] = array(
    'submit' => array('#type' => 'submit', '#value' => t('Save bundle')),
    '#weight' => 10,
  );

  // Fill in default values when an existing bundle is edited.
  if (isset($bundle['kind'])) {
    $form['kind']['#disabled'] = TRUE;
    $form['kind']['#default_value'] = $bundle['kind'];
    $form['kind']['#description'] .= ' <em>'. t("Note: The bundle type can't be changed after creation.") .'</em>';
    $form['bid'] = array('#type' => 'value', '#value' => $bundle['bid']);

    // Show files in this bundle.
    module_load_include('list.inc', 'sf_cache_ui', 'sf_cache_ui');
    $files = sf_cache_ui_files_by_bundle($form_values, $bundle['kind']);

    $form['files'] = $files['bundles'][$bundle['bid']];
    $form['files']['#title'] = t('Files in this bundle');
    $form['files']['#columns'] = array(
      'file_indicator' => TRUE,
      'file_name' => TRUE,
      'file_size' => TRUE,
      'file_options' => TRUE,
    );
    $form['files']['#empty'] = t('There are no files in this bundle.');
    // Include the table functions and set the correct theme function.
    module_load_include('table.inc', 'sf_cache_ui', 'sf_cache_ui');
    $form['files']['#theme'] = 'sf_cache_ui_table';
    $form['files']['#table_type'] = 'file-by-bundle-read-only';

    // Add a delete link.
    $form['buttons']['delete'] = array('#value' => l(t('Delete'), 'admin/build/sf_cache/bundle/'. $bundle['bid'] .'/delete', array('query' => drupal_get_destination())));
  }

  return $form;
}

/**
 * Submit callback; saves a bundle edited in a form.
 */
function sf_cache_ui_bundle_edit_submit($form, &$form_state) {
  if (!isset($form_state['values']['bid'])) {
    sf_cache_bundle_create(array(
      'kind' => $form_state['values']['kind'],
      'title' => $form_state['values']['title'],
      'minify' => $form_state['values']['minify'],
    ));
    drupal_set_message(t('The bundle %title has been created.', array('%title' => $form_state['values']['title'])));
  }
  else {
    $bundle = sf_cache_bundle_save($form_state['values']);

    if ($bundle['#updated']) {
      drupal_set_message(t('The bundle %title has been updated.', array('%title' => $form_state['values']['title'])));
    }
    else {
      drupal_set_message(t('There were no changes to the bundle %title.', array('%title' => $form_state['values']['title'])));
    }
  }

  $form_state['redirect'] = 'admin/build/sf_cache/bundle';
}

/**
 * Menu callback; shows a confirmation page before deleting a bundle.
 */
function sf_cache_ui_bundle_delete(&$form_values, $bundle) {
  $form = array(
    'bid' => array('#type' => 'value', '#value' => $bundle['bid']),
  );

  return confirm_form($form,
     t('Are you sure you want to delete the bundle %title?', array('%title' => $bundle['title'])),
     isset($_GET['destination']) ? $_GET['destination'] : 'admin/build/sf_cache/bundle',
     t('All files in this bundle will become standalone. This action cannot be undone.'),
     t('Delete'), t('Cancel'));
}

/**
 * Submit callback; handles the deletion of a bundle.
 */
function sf_cache_ui_bundle_delete_submit($form, &$form_state) {
  $bundle = sf_cache_bundle_load($form_state['values']['bid']);
  sf_cache_bundle_delete($form_state['values']['bid']);
  drupal_set_message(t('The bundle %title has been deleted.', array('%title' => $bundle['title'])));

  // Update all files accordingly.
  sf_cache_ui_submit_update();

  $form_state['redirect'] = 'admin/build/sf_cache/bundle';
}
