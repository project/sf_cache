<?php

/**
 * @file
 * Contains configuration forms for Support File Cache.
 */

/**
 * Menu callback; Displays the module's settings page.
 */
function sf_cache_ui_config_settings(&$form_values) {
  $form = array();

  $form['sf_cache_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
    '#description' => t('Controls whether the replacement of support files through their bundles will be performed.'),
    '#default_value' => variable_get('sf_cache_enabled', FALSE),
  );

  $themes = array();
  foreach (list_themes() as $theme) {
    if ($theme->status) {
      $themes[$theme->name] = $theme->name;
    }
  }

  // Only display the theme selection when there is more than one theme.
  if (count($themes) > 1) {
    ksort($themes);

    // Indicate the default theme.
    $default_theme = variable_get('theme_default', 'garland');
    $themes[$default_theme] = t('@theme-name (default)', array('@theme-name' => $default_theme));

    // Add an admin theme checkbox.
    $admin_theme = variable_get('admin_theme', '0');
    if ($admin_theme == '0') {
      $themes[SF_CACHE_ADMIN_THEME] = t('<em>Administration theme</em> (currently using system default)');
    }
    else {
      $themes[SF_CACHE_ADMIN_THEME] = t('<em>Administration theme</em> (currently set to %admin-theme)', array('%admin-theme' => $admin_theme));
    }

    $form['sf_cache_enabled_themes'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Only enable for these themes'),
      '#description' => t('If at least one theme is checked, replacements will only take place for the selected themes. If no themes are checked, replacement will happen for all themes.'),
      '#options' => $themes,
      '#default_value' => variable_get('sf_cache_enabled_themes', array()),
      '#disabled' => (count($themes) == 1),
      '#prefix' => '<div class="sf-cache-enabled-themes">',
      '#suffix' => '</div>',
    );
  }

  $form['sf_cache_autoupdate'] = array(
    '#type' => 'checkbox',
    '#title' => t('Automatically update'),
    '#description' => t('If enabled, changes to the configuration will automatically lead to an update of the files. Otherwise, changes will only be performed on demand.'),
    '#default_value' => variable_get('sf_cache_autoupdate', TRUE),
  );

  $form['sf_cache_logging'] = array(
    '#type' => 'checkbox',
    '#title' => t('Logging'),
    '#description' => t('Specifies whether the logging mode for support files is enabled or not.'),
    '#default_value' => variable_get('sf_cache_logging', FALSE),
  );

  $form['sf_cache_use_inode'] = array(
    '#type' => 'checkbox',
    '#title' => t('Track changes with inode number'),
    '#description' => t('If checked, the inode number of CSS and JavaScript files are used to check whether they have changed. You should disable that in multi-server setups since inode numbers vary from filesystem to filesystem.'),
    '#default_value' => variable_get('sf_cache_use_inode', TRUE),
  );

  $form['revision'] = array(
    '#type' => 'fieldset',
    '#title' => t('Revision numbers'),
  );

  $form['revision']['sf_cache_revision'] = array(
    '#type' => 'textfield',
    '#title' => t('Revision'),
    '#description' => t('A revision number for support files.'),
    '#size' => 8,
    '#maxlength' => 8,
    '#default_value' => variable_get('sf_cache_revision', '1'),
  );

  $form['revision']['sf_cache_graphics_revision'] = array(
    '#type' => 'radios',
    '#title' => t('Graphics revision numbers'),
    '#description' => t('Controls whether revision numbers are appended to graphic files and it what manner.'),
    '#options' => array(
      SF_CACHE_GRAPHICS_REV_NONE => t('None'),
      SF_CACHE_GRAPHICS_REV_QUERY => t('Query string'),
      SF_CACHE_GRAPHICS_REV_FILENAME => t('End of filename'),
    ),
    '#default_value' => variable_get('sf_cache_graphics_revision', SF_CACHE_GRAPHICS_REV_NONE),
  );

  $form['sf_cache_security_hash'] = array(
    '#type' => 'textfield',
    '#title' => t('Security hash'),
    '#description' => t('An arbitrary string that is used as key to invoke updates. Visit <a href="@url">@url</a> to request an update. Instead of "update", you can also use "detect" and "check".', array('@url' => url('admin/build/sf_cache/api/update/'. variable_get('sf_cache_security_hash', md5(''))))),
    '#size' => 32,
    '#default_value' => variable_get('sf_cache_security_hash', md5(microtime())),
    '#maxlength' => 32,
  );

  $form['#submit'] = array(
    'system_settings_form_submit',
    'sf_cache_ui_submit_update'
  );

  return system_settings_form($form);
}

/**
 * Menu callback; Displays the path configuration page.
 */
function sf_cache_ui_config_paths(&$form_values) {
  $form = array();

  $css = variable_get('sf_cache_css_target', 'sf_css');
  $css_dir = file_create_path($css);
  file_check_directory($css_dir, FILE_CREATE_DIRECTORY, 'sf_cache_css_target');

  $form['css']['external'] = array(
    '#type' => 'fieldset',
    '#title' => t('CSS'),
  );

  $form['css']['external']['sf_cache_css_source'] = array(
    '#type' => 'textfield',
    '#title' => t('Source directory'),
    '#description' => t('The base directory all CSS files reside in. This path is relative to the Drupal installation. In a regular installation, you should leave this value as empty.'),
    '#default_value' => variable_get('sf_cache_css_source', ''),
    '#prefix' => '<div class="sf-cache-form-half clear-block">',
  );

  $form['css']['external']['sf_cache_css_target'] = array(
    '#type' => 'textfield',
    '#title' => t('Target directory'),
    '#description' => t('A directory within the files directory of your site in which bundled CSS files will be stored.'),
    '#default_value' => $css,
    '#field_prefix' => check_plain(file_directory_path() .'/'),
    '#suffix' => '</div>',
  );

  $form['css']['external']['sf_cache_css_server'] = array(
    '#type' => 'textfield',
    '#title' => t('URL prefix'),
    '#description' => t('The URL where the generated CSS files are accessible. If empty, the regular files directory is used. Omit leading and trailing slashes.'),
    '#default_value' => variable_get('sf_cache_css_server', ''),
    '#field_prefix' => 'http(s)://',
  );

  $form['css']['external']['sf_cache_graphics_server'] = array(
    '#type' => 'textfield',
    '#title' => t('Graphics prefix'),
    '#description' => t('The URL where CSS graphic files are accessible. If empty, the regular Drupal root directory is used. Omit leading and trailing slashes.'),
    '#default_value' => variable_get('sf_cache_graphics_server', ''),
    '#field_prefix' => 'http(s)://',
  );


  $js = variable_get('sf_cache_js_target', 'sf_js');
  $js_dir = file_create_path($js);
  file_check_directory($js_dir, FILE_CREATE_DIRECTORY, 'sf_cache_js_target');

  $form['js'] = array(
    '#type' => 'fieldset',
    '#title' => t('JavaScript'),
  );

  $form['js']['sf_cache_js_source'] = array(
    '#type' => 'textfield',
    '#title' => t('Source directory'),
    '#description' => t('The base directory all JavaScript files reside in. This path is relative to the Drupal installation. In a regular installation, you should leave this value as empty.'),
    '#default_value' => variable_get('sf_cache_js_source', ''),
    '#prefix' => '<div class="sf-cache-form-half clear-block">',
  );

  $form['js']['sf_cache_js_target'] = array(
    '#type' => 'textfield',
    '#title' => t('Target directory'),
    '#description' => t('A directory within the files directory of your site in which bundled JavaScript files will be stored.'),
    '#default_value' => $js,
    '#field_prefix' => check_plain(file_directory_path() .'/'),
    '#suffix' => '</div>',
  );

  $form['js']['sf_cache_js_server'] = array(
    '#type' => 'textfield',
    '#title' => t('URL prefix'),
    '#description' => t('The URL where the generated JavaScript files are accessible. If empty, the regular files directory is used. Omit the trailing slash.'),
    '#default_value' => variable_get('sf_cache_js_server', ''),
    '#field_prefix' => 'http(s)://',
  );

  $form['#submit'] = array(
    'system_settings_form_submit',
    'sf_cache_ui_config_paths_submit',
    'sf_cache_ui_submit_update',
  );

  return system_settings_form($form);
}

/**
 * Submit callback; additionally ensures that the specified paths exist.
 */
function sf_cache_ui_config_paths_submit($form, &$form_state) {
  // Make sure that the paths exist.
  file_create_path(variable_get('sf_cache_css_target', 'sf_css'));
  file_create_path(variable_get('sf_cache_js_target', 'sf_js'));
}

/**
 * Menu callback; displays the maintenance page.
 */
function sf_cache_ui_config_maintenance(&$form_values) {
  $form = array();
  global $user;

  $form['file_section'] = array(
    '#value' => t('You can perform the following tasks:'),
  );

  $form['detect'] = array(
    '#type' => 'submit',
    '#value' => t('Detect new files'),
    '#prefix' => '<div class="sf-cache-extended-submit clear-block">',
    '#suffix' => '<div class="description">'. t('Looks for new files which are not yet registered with Support File Cache.') .'</div></div>',
    '#submit' => array('sf_cache_ui_config_execute_detect'),
  );

  $form['check'] = array(
    '#type' => 'submit',
    '#value' => t('Find changes in files'),
    '#prefix' => '<div class="sf-cache-extended-submit clear-block">',
    '#suffix' => '<div class="description">'. t('Checks all know files for changes happened since the last check. Updates the database record of the file accordingly.') .'</div></div>',
    '#submit' => array('sf_cache_ui_config_execute_check'),
  );

  $form['update'] = array(
    '#type' => 'submit',
    '#value' => t('Update bundles'),
    '#prefix' => '<div class="sf-cache-extended-submit clear-block">',
    '#suffix' => '<div class="description">'. t('Creates new processed files for all files that changed since the last time bundles were updated. Deletes and creates files on the file system.') .'</div></div>',
    '#submit' => array('sf_cache_ui_config_execute_update'),
  );

  $form['cleanup_section'] = array(
    '#value' => t('The following items let you perform cleanup tasks:'),
  );

  $form['prune_statistics'] = array(
    '#type' => 'submit',
    '#value' => t('Prune statistical data'),
    '#prefix' => '<div class="sf-cache-extended-submit clear-block">',
    '#suffix' => '<div class="description">'. t('This will delete all statistical data collected by Support File Cache.') . (' '. (variable_get('sf_cache_logging', FALSE) ? t('<strong>Note</strong>: Logging is enabled and will continue to collect data.') : '')) .'</div></div>',
    '#submit' => array('sf_cache_ui_config_maintenance_prune_statistics'),
  );

  $empty_bundles = (int)db_result(db_query("SELECT COUNT(*) FROM (SELECT b.bid, COUNT(f.fid) as num FROM {sf_cache_bundle} b LEFT JOIN {sf_cache_file} f ON b.bid = f.bid GROUP BY b.bid HAVING num = 0) as tbl"));
  $form['delete_empty_bundles'] = array(
    '#type' => 'submit',
    '#disabled' => !$empty_bundles,
    '#value' => $empty_bundles ? format_plural($empty_bundles, 'Delete 1 empty bundle', 'Delete @count empty bundles') : t('Delete empty bundles'),
    '#prefix' => '<div class="sf-cache-extended-submit clear-block">',
    '#suffix' => '<div class="description">'. t('This will delete all bundles with no associated files. Using the autodetect feature can leave you with several of these.') .'</div></div>',
    '#submit' => array('sf_cache_ui_config_maintenance_delete_empty_bundles'),
  );

  $orphaned_files = (int)db_result(db_query("SELECT COUNT(*) FROM {sf_cache_file} WHERE hash = ''"));
  $form['delete_orphaned_files'] = array(
    '#type' => 'submit',
    '#disabled' => !$orphaned_files,
    '#value' => $orphaned_files ? format_plural($orphaned_files, 'Delete 1 orphaned file', 'Delete @count orphaned files') : t('Delete orphaned files'),
    '#prefix' => '<div class="sf-cache-extended-submit clear-block">',
    '#suffix' => '<div class="description">'. t('This will delete all database records of files which can\'t be found anymore. You can see which file records are orphaned by visiting the CSS or JavaScript tab and looking for files with a "file not found" indicator.') .'</div></div>',
    '#submit' => array('sf_cache_ui_config_maintenance_delete_orphaned'),
  );

  $form['reset'] = array(
    '#type' => 'submit',
    '#value' => t('Reset module'),
    '#disabled' => TRUE,
    '#prefix' => '<div class="sf-cache-extended-submit clear-block">',
    '#suffix' => '<div class="description">'. t('To entirely reset the module, disable, uninstall and reenable it using the <a href="@module-admin">module administration</a> page.', array('@module-admin' => url('admin/build/modules'))) .'</div></div>',
  );

  return $form;
}

/**
 * Submit button callback; cleans the statistics table.
 */
function sf_cache_ui_config_maintenance_prune_statistics($form, &$form_state) {
  db_query("DELETE FROM {sf_cache_file_statistics}");
  drupal_set_message(t('The file statistics table has been cleared.'));
}

/**
 * Submit button callback; deletes orphaned files form the database.
 */
function sf_cache_ui_config_maintenance_delete_empty_bundles($form, &$form_state) {
  $deleted = sf_cache_bundle_delete_empty();
  if (empty($deleted)) {
    drupal_set_message(t('There were no empty bundles.'));
  }
  else {
    drupal_set_message(format_plural(count($deleted),
      'The following empty bundle has been deleted:',
      'The following empty bundles have been deleted:') .' '.
      theme('item_list', $deleted));
  }
}

/**
 * Submit button callback; deletes orphaned files form the database.
 */
function sf_cache_ui_config_maintenance_delete_orphaned($form, &$form_state) {
  $deleted = sf_cache_file_delete_orphaned();
  if (empty($deleted)) {
    drupal_set_message(t('There were no orphaned files.'));
  }
  else {
    drupal_set_message(format_plural(count($deleted),
      'The following orphaned file has been deleted:',
      'The following orphaned files have been deleted:') .' '.
      theme('item_list', $deleted));
  }
}

/**
 * Menu callback; displays the page for managing excluded paths.
 */
function sf_cache_ui_config_excludes() {
  $output = '';

  $excludes = variable_get('sf_cache_excludes', array());

  $rows = array();
  foreach ($excludes as $path) {
    $row = array();
    $row[] = (!file_exists('./'. $path)) ? array('class' => 'sf-cache-file-missing', 'title' => t('This file or directory does not exist.')) : '';
    $row[] = check_plain($path) . (is_dir('./'. $path) ? '/*.*' : '');
    $row[] = l(t('delete'), 'admin/build/sf_cache/config/excludes/delete', array('query' => 'path='. drupal_urlencode($path)));
    $rows[] = $row;
  }

  if (empty($rows)) {
    $rows[] = array(
      array(
        'colspan' => 3,
        'data' => t('There are currently no excludes.'),
      ),
    );
  }

  // Add the form that allows addition of new excludes.
  $rows[] = array(
    '',
    array(
      'colspan' => 2,
      'data' => drupal_get_form('sf_cache_ui_config_excludes_add_form'),
    ),
  );

  return theme('table', array('', t('Path'), t('Options')), $rows);
}

/**
 * Form callback; generates the form for adding a new exclude path.
 */
function sf_cache_ui_config_excludes_add_form(&$form_state) {
  $form = array();

  $form['path'] = array(
    '#type' => 'textfield',
    '#description' => t('Enter a path you want to exclude.'),
    '#size' => 40,
    '#maxlength' => 255,
  );

  $form['submit'] =  array(
    '#type' => 'submit',
    '#value' => t('Add exclude'),
    '#submit' => array('sf_cache_ui_config_excludes_add'),
  );

  return $form;
}

/**
 * Submit button callback; handles the addition of new exclude paths.
 */
function sf_cache_ui_config_excludes_add($form, &$form_state) {
  if (!empty($form_state['values']['path'])) {
    if ($files = sf_cache_exclude_add(trim($form_state['values']['path'], ' /'))) {
      if (empty($files)) {
        drupal_set_message(t('The exclude path has been added but no files were affected.'));
      }
      else {
        drupal_set_message(format_plural(count($files), 'The following path is now excluded:', 'The following paths are now excluded:') .' '. theme('item_list', $files));
      }
    }
  }
}

/**
 * Menu callback; displays a confirmation page before remvoing exclude paths.
 */
function sf_cache_ui_config_excludes_delete(&$form_values) {
  $form = array();
  $excludes = variable_get('sf_cache_excludes', array());
  $path = trim($_REQUEST['path'], ' /');

  if (in_array($path, $excludes)) {
    $form['path'] = array('#type' => 'value', '#value' => $path);

    return confirm_form($form,
       t('Are you sure you want to remove the exclude %path?', array('%path' => $path)),
       isset($_GET['destination']) ? $_GET['destination'] : 'admin/build/sf_cache/config/excludes',
       t('Support File Cache will index that path again.'),
       t('Delete'), t('Cancel'));
  }
}

/**
 * Submit callback; removes an exclude path from the list.
 */
function sf_cache_ui_config_excludes_delete_submit($form, &$form_state) {
  if (sf_cache_exclude_remove($form_state['values']['path'])) {
    drupal_set_message(t('The exclude path %path has been removed.', array('%path' => $form_state['values']['path'])));
  }

  $form_state['redirect'] = 'admin/build/sf_cache/config/excludes';
}

function sf_cache_ui_config_minify(&$form_state) {
  drupal_add_js(drupal_get_path('module', 'sf_cache_ui') .'/sf_cache_ui.js');

  $form = array();

  $form['css'] = array(
    '#type' => 'fieldset',
    '#title' => t('CSS'),
  );

  $form['css']['sf_cache_minify_css_mode'] = array(
    '#type' => 'radios',
    '#title' => t('Minification mode'),
    '#description' => t('Defines how CSS should be minified.'),
    '#options' => array(
      SF_CACHE_MINIFICATION_DISABLED => t('No minification'),
      SF_CACHE_MINIFICATION_BUILTIN => t('Built-in'),
      SF_CACHE_MINIFICATION_EXTERNAL => t('External'),
    ),
    '#default_value' => variable_get('sf_cache_minify_css_mode', SF_CACHE_MINIFICATION_BUILTIN),
  );

  $form['css']['external'] = array(
    '#prefix' => '<div id="sf-cache-css-external">',
    '#suffix' => '</div>',
  );

  $form['css']['external']['sf_cache_minify_css_executable'] = array(
    '#type' => 'textfield',
    '#title' => t('Executable'),
    '#description' => t('The path to the jar file to execute.'),
    '#default_value' => variable_get('sf_cache_minify_css_executable', ''),
  );

  $form['css']['external']['sf_cache_minify_css_parameters'] = array(
    '#type' => 'textarea',
    '#title' => t('Parameters'),
    '#description' => t('Specify parameters for the executable. Line feeds will be removed, so you can specify one parameter per line.'),
    '#cols' => 60,
    '#rows' => 5,
    '#default_value' => variable_get('sf_cache_minify_css_parameters', ''),
  );


  $form['js'] = array(
    '#type' => 'fieldset',
    '#title' => t('JavaScript'),
  );

  $form['js']['sf_cache_minify_js_mode'] = array(
    '#type' => 'radios',
    '#title' => t('Minification mode'),
    '#description' => t('Defines how JavaScript should be minified.'),
    '#options' => array(
      SF_CACHE_MINIFICATION_DISABLED => t('No minification'),
      SF_CACHE_MINIFICATION_JSMIN => t('JSMin'),
      SF_CACHE_MINIFICATION_EXTERNAL => t('External'),
    ),
    '#process' => array('expand_radios', 'sf_cache_ensure_jsmin'),
    '#default_value' => variable_get('sf_cache_minify_js_mode', SF_CACHE_MINIFICATION_DISABLED),
  );

  $form['js']['external'] = array(
    '#prefix' => '<div id="sf-cache-js-external">',
    '#suffix' => '</div>',
  );

  $form['js']['external']['sf_cache_minify_js_executable'] = array(
    '#type' => 'textfield',
    '#title' => t('Executable'),
    '#description' => t('The path to the jar file to execute.'),
    '#default_value' => variable_get('sf_cache_minify_js_executable', ''),
  );

  $form['js']['external']['sf_cache_minify_js_parameters'] = array(
    '#type' => 'textarea',
    '#title' => t('Parameters'),
    '#description' => t('Specify parameters for the executable. Line feeds will be removed, so you can specify one parameter per line.'),
    '#cols' => 60,
    '#rows' => 5,
    '#default_value' => variable_get('sf_cache_minify_js_parameters', ''),
  );


  $form['#submit'] = array(
    'system_settings_form_submit',
    'sf_cache_ui_submit_update',
  );

  return system_settings_form($form);
}

function sf_cache_ensure_jsmin($element) {
  if (isset($element[SF_CACHE_MINIFICATION_JSMIN]) && !sf_cache_jsmin_available()) {
    $element[SF_CACHE_MINIFICATION_JSMIN]['#disabled'] = TRUE;
    $element[SF_CACHE_MINIFICATION_JSMIN]['#description'] = t('Copy the <a href="@url">JSMin PHP file</a> into the Support File Cache folder to enable JSMin. The PHP file has to start with "jsmin-".', array('@url' => 'http://code.google.com/p/jsmin-php/'));
  }

  return $element;
}
