<?php

/**
 * @file
 * Contains the user interface for display and managing the file lists.
 */

/**
 * Sorts an array of arrays by specified indexes.
 *
 * @param $array
 *   The array that should be sorted.
 * @param $key
 *   Either a key of the arrays in $array or an array of keys to sort after.
 */
function array_key_sort(&$array, $key = 'weight') {
  _array_key_sort_compare(NULL, NULL, $key);
  uasort($array, '_array_key_sort_compare');
}

/**
 * Helper function for array_key_sort().
 */
function _array_key_sort_compare($a, $b, $key = NULL) {
  static $keys;

  if (isset($key)) {
    $keys = (!is_array($key)) ? array($key) : $key;
  }
  else {
    $key = reset($keys);
    do {
      $a_idx = (is_array($a) && isset($a[$key])) ? $a[$key] : 0;
      $b_idx = (is_array($b) && isset($b[$key])) ? $b[$key] : 0;
    } while ($a_idx == $b_idx && $key = next($keys));

    return ($a_idx < $b_idx) ? -1 : (($a_idx == $b_idx) ? 0 : 1);
  }
}




/**
 * Menu callback; Display files grouped by bundles.
 */
function sf_cache_ui_files_by_bundle(&$form_values, $kind) {
  $form = array();

  // Load bundles.
  $bundles = sf_cache_bundle_load();
  if (!isset($bundles[$kind])) {
    $bundles[$kind] = array();
  }
  $bundles = $bundles[$kind];
  $bundles[SF_CACHE_UNBUNDLED] = array('bid' => SF_CACHE_UNBUNDLED, 'title' => '<'. t('standalone') .'>');

  // Prepare the options array for the bundle selectors.
  $bundles_select = array();
  foreach ($bundles as $bid => $bundle) {
    $bundles_select[$bid] = $bundle['title'];
  }

  // Load the files and flush them before so that changes are reflected.
  sf_cache_check_files();
  $files = sf_cache_file_load('fid', TRUE);
  $files = $files[$kind];

  // Sort the files into bundles.
  $bundled_files = array();
  foreach ($files as $file) {
    if (!isset($file['bid']) || !isset($bundles[$file['bid']])) {
      $file['bid'] = SF_CACHE_UNBUNDLED;
    }
    if (!isset($bundled_files[$file['bid']])) {
      $bundled_files[$file['bid']] = array();
    }
    $file['directory'] = dirname($file['filepath']);
    $file['filename'] = basename($file['filepath']);
    $bundled_files[$file['bid']][$file['filepath']] = $file;
  }
  unset($files);

  $form['bundles'] = array('#tree' => TRUE);

  // Build the table.
  foreach ($bundles as $bid => $bundle) {
    $form['bundles'][$bid] = array(
      '#tree' => TRUE,
      '#bundle' => $bundle,
      '#theme' => 'sf_cache_ui_file_table',
      '#table_type' => 'file-by-bundle',
    );

    $kinds = array('general' => 0, 'theme' => 0);

    if (isset($bundled_files[$bid])) {
      array_key_sort($bundled_files[$bid], array('weight', 'directory', 'filename'));
      foreach ($bundled_files[$bid] as $filepath => $file) {
        $item = array(
          '#file' => $file,
          'enabled' => array(
            '#type' => 'checkbox',
            '#default_value' => $file['enabled'],
            '#disabled' => empty($file['hash']),
          ),
          'minify' => array(
            '#type' => 'checkbox',
            '#default_value' => $file['minify'],
            '#attributes' => array(
              'title' => t('Check to minify this file.'),
            ),
          ),
          'bid' => array(
            '#type' => 'select',
            '#options' => $bundles_select,
            '#default_value' => $bid,
          ),
        );

        if ($bid != SF_CACHE_UNBUNDLED) {
          // Count theme and other files.
          if (preg_match('@^(sites/[^/]+/)?themes/@i', $file['filepath'])) {
            $kinds['theme']++;
          }
          else {
            $kinds['general']++;
          }

          $item['weight'] = array(
            '#type' => 'weight',
            '#delta' => 5,
            '#default_value' => $file['weight'],
          );
        }

        $form['bundles'][$bid][$file['fid']] = $item;
      }
    }

    if ($kinds['general'] > 0 && $kinds['theme'] > 0) {
      $form['bundles'][$bid]['#caption'] = '<div class="warning">'. t('<strong>Warning</strong>: This bundle contains both theme-related and generic files. This may lead to weird effects when you have enabled multiple themes on your site, because a theme might load another theme\'s files via a bundle.') .'</div>';
    }
  }

  $form['kind'] = array('#type' => 'value', '#value' => $kind);

  $form['buttons'] = array(
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Save configuration'),
    )
  );

  return $form;
}

/**
 * Theme used by bundle displays.
 */
function theme_sf_cache_ui_file_table($element) {
  if (isset($element['#bundle'])) {
    $element['#title'] = ($element['#bundle']['bid'] == SF_CACHE_UNBUNDLED) ?
                           '<'. t('Standalone') .'>' : $element['#bundle']['title'];
  }

  $element['#columns'] = array(
    'file_enabled' => TRUE,
    'file_name' => TRUE,
    'bundle_selector' => TRUE,
    'weight_selector' => TRUE,
    'minify' => TRUE,
    'file_size' => TRUE,
    'file_options' => TRUE,
  );

  if (!isset($element['#bundle']) || $element['#bundle']['bid'] == SF_CACHE_UNBUNDLED) {
    unset($element['#columns']['weight_selector']);
  }
  else {
    unset($element['#columns']['minify']);
  }

  $element['#empty'] = t('There are no files in this bundle.');

  // Include the table functions and render the table.
  module_load_include('table.inc', 'sf_cache_ui', 'sf_cache_ui');
  return theme('sf_cache_ui_table', $element);
}

/**
 * Submit callback; saves the new bundle associated with each file.
 */
function sf_cache_ui_files_by_bundle_submit($form, &$form_state) {
  $files = sf_cache_file_load('fid', TRUE);
  $files = $files[$form_state['values']['kind']];

  $changed = array();

  // Detect changes in the bundle.
  foreach ($form_state['values']['bundles'] as $bid => $bundled_files) {
    foreach ($bundled_files as $fid => $file) {
      $file += $files[$fid];
      $file = sf_cache_file_save($file, NULL, FALSE);
      if ($file['#updated']) {
        $changed[] = check_plain($file['filepath']);
      }
    }
  }

  if (!empty($changed)) {
    $changed = theme('item_list', $changed);
    drupal_set_message(t('Changes to following files have been saved: !list', array('!list' => $changed)));
  }
  else {
    drupal_set_message(t('No changes to files were made.'));
  }

  // Update all files accordingly.
  sf_cache_ui_submit_update();
}

/**
 * Menu callback; Display files ordered by path.
 */
function sf_cache_ui_files_by_path(&$form_values, $kind) {
  $form = array();

  // Load the bundles.
  $bundles_select = _sf_cache_ui_bundles_dropdown($file['kind']);

  // Load the files and flush them before so that changes are reflected.
  sf_cache_check_files();
  $files = sf_cache_file_load('fid', TRUE);
  $files = $files[$kind];

  // Prepare the files array
  foreach ($files as $fid => $file) {
    if (!isset($file['bid']) || !isset($bundles_select[$file['bid']])) {
      $files[$fid]['bid'] = SF_CACHE_UNBUNDLED;
    }
    $files[$fid]['directory'] = dirname($file['filepath']);
    $files[$fid]['filename'] = basename($file['filepath']);
  }
  // Sort the files by directory and then filename. (sorting on filepath results
  // in wrong sorting).
  array_key_sort($files, array('directory', 'filename'));

  // Build the table.
  $form['files'] = array(
    '#tree' => TRUE,
    '#theme' => 'sf_cache_ui_file_table',
    '#table_type' => 'file-by-path',
  );

  // Add the rows for the table.
  foreach ($files as $fid => $file) {
    $form['files'][$fid] = array(
      '#file' => $file,
      'enabled' => array(
        '#type' => 'checkbox',
        '#default_value' => $file['enabled'],
        '#disabled' => empty($file['hash']),
      ),
      'minify' => ($file['bid'] == SF_CACHE_UNBUNDLED) ? array(
          '#type' => 'checkbox',
          '#default_value' => $file['minify'],
        ) : array('#value' => ''),
      'bid' => array(
        '#type' => 'select',
        '#options' => $bundles_select,
        '#default_value' => $file['bid'],
      ),
    );
  }

  $form['kind'] = array('#type' => 'value', '#value' => $kind);

  $form['buttons'] = array(
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Save configuration'),
    )
  );

  return $form;
}

/**
 * Submit callback; saves the new bundle associated with each file.
 */
function sf_cache_ui_files_by_path_submit($form, &$form_state) {
  $files = sf_cache_file_load('fid', TRUE);
  $files = $files[$form_state['values']['kind']];

  $changed = array();

  // Detect changes in the bundle.
  foreach ($form_state['values']['files'] as $fid => $file) {
    if ($files[$fid]['bid'] != $file['bid']) {
      $file = array_merge($files[$fid], $file);
      sf_cache_file_save($file, NULL, FALSE);
      if ($file['#updated']) {
        $changed[] = check_plain($file['filepath']);
      }
    }
  }

  if (!empty($changed)) {
    $changed = theme('item_list', $changed);
    drupal_set_message(t('Changes to following files have been saved: !list', array('!list' => $changed)));
  }
  else {
    drupal_set_message(t('No changes to files were made.'));
  }

  // Update all files accordingly.
  sf_cache_ui_submit_update();
}

/**
 * Menu callback; Tries to determine bundles based on collected statistics.
 */
function sf_cache_ui_autodetect(&$form_values, $kind, $threshold = 100) {
  $statistics_count = db_result(db_query('SELECT COUNT(*) FROM {sf_cache_file_statistics}'));
  if ($statistics_count < 5000) {
    drupal_set_message(t('Support File Cache couldn\'t collect enough statistics so far. <a href="@url">Enable statistics</a> and let your users browse the site to gather more statistics. You need @count more file records.', array('@url' => url('admin/build/sf_cache/config'), '@count' => 5000 - $statistics_count)), 'warning');
    return;
  }

  $form = array();
  $threshold = (int)$threshold;
  sf_cache_check_files();

  // Load the files and flush them before so that changes are reflected.
  $files = sf_cache_file_load('fid', TRUE);
  $bundles = sf_cache_bundle_load();

  // Get suggestions
  $suggestions = sf_cache_bundle_suggest($threshold, $kind, TRUE);

  $form['bundles'] = array('#tree' => TRUE);

  $i = 1;
  foreach ($suggestions[$kind] as $id => $bundle) {
    $form['bundles'][$id] = array(
      '#tree' => TRUE,
      '#bundle' => array(
        'bid' => $id,
        'title' => t('New Bundle #@count', array('@count' => $i++)),
      ),
      '#theme' => 'sf_cache_ui_autodetect_table',
      '#table_type' => 'file-autodetect',
    );

    foreach ($bundle as $fid => $filepath) {
      $file = $files[$kind][$fid];

      if ($file['bid'] == SF_CACHE_UNBUNDLED && $id == SF_CACHE_UNBUNDLED) {
        continue;
      }

      // Don't show minified file sizes in the autodetect listing.
      $file['minify'] = FALSE;

      $form['bundles'][$id][$fid] = array(
        '#file' => $file,
        '#bundle_title' => ($file['bid'] == SF_CACHE_UNBUNDLED ? t('<standalone>') : $bundles[$kind][$file['bid']]['title']),
        'checked' => array(
          '#type' => 'checkbox',
          '#default_value' => TRUE,
          '#attributes' => array('title' => t('Include this file in the bundle.')),
        ),
      );
    }
  }

  $form['kind'] = array('#type' => 'value', '#value' => $kind);

  $form['buttons'] = array(
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Save suggestion'),
    )
  );

  return $form;
}

/**
 * Theme a table on the autodetect page.
 */
function theme_sf_cache_ui_autodetect_table($element) {
  $element['#title'] = ($element['#bundle']['bid'] == SF_CACHE_UNBUNDLED) ? '<'. t('Standalone') .'>' : $element['#bundle']['title'];
  $element['#columns'] = array(
    'file_indicator' => TRUE,
    'file_checked' => TRUE,
    'file_name' => TRUE,
    'current_bundle' => TRUE,
    'file_size' => TRUE,
  );

  // Include the table functions and render the table.
  module_load_include('table.inc', 'sf_cache_ui', 'sf_cache_ui');
  return theme('sf_cache_ui_table', $element);
}

/**
 * Submit callback; saves bundles from the autodetect page.
 */
function sf_cache_ui_autodetect_submit($form, &$form_state) {
  $kind = $form_state['values']['kind'];
  $files = sf_cache_file_load('fid', TRUE);

  foreach ($form_state['values']['bundles'] as $id => $bundle_files) {
    // Create a new bundle and name it properly.
    $bundle = sf_cache_bundle_create(array('kind' => $kind));

    $bundle['title'] = t('Bundle #@count', array('@count' => $bundle['bid']));
    // Save the updated title.
    sf_cache_bundle_save($bundle, FALSE);

    foreach ($bundle_files as $fid => $file) {
      if ($file['checked']) {
        $files[$kind][$fid]['bid'] = ($id === -1) ? FALSE : $bundle['bid'];
        sf_cache_file_save($files[$kind][$fid], NULL, FALSE);
      }
    }
  }

  drupal_set_message(t('The new bundles have been created.'));

  // Update all files accordingly.
  sf_cache_ui_submit_update();

  return 'admin/build/sf_cache/'. $kind;
}
