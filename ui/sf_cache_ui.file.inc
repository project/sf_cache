<?php

// Include the generic admin API functions and the bundle functions.
module_load_include('admin.inc', 'sf_cache');
module_load_include('bundle.inc', 'sf_cache_ui');

/**
 * @file
 * Contains the user interface for managing files.
 */

function sf_cache_ui_file_edit(&$form_state, $file) {
  $form = array();

  if (!empty($file['processed'])) {
    $title_description = t('The path of the file. (<a href="@source">source</a>, <a href="@target">target</a>)', array('@source' => sf_cache_url(sf_cache_file_path($file['filepath'], $file['kind'])), '@target' => sf_cache_url($file['processed'])));
  }
  else {
    $title_description = t('The path of the file. (<a href="@source">source</a>)', array('@source' => sf_cache_url(sf_cache_file_path($file['filepath'], $file['kind']))));
  }

  $form['title'] = array(
    '#type' => 'sf_cache_ui_datafield',
    '#title' => t('Path'),
    '#description' => $title_description,
    '#value' => '<h2 class="sf-cache-file-title">'. $file['filepath'] .'</h2>',
  );

  $form['kind'] = array(
    '#type' => 'sf_cache_ui_datafield',
    '#title' => t('Type'),
    '#description' => t('The type of this file.'),
    '#value' => $file['kind'] == 'css' ? t('CSS') : t('JavaScript'),
  );

  // Get a list of bundles for the selector.
  $bundles_select = _sf_cache_ui_bundles_dropdown($file['kind']);

  $form['bid'] = array(
    '#type' => 'select',
    '#title' => t('Associated bundle'),
    '#description' => t('The bundle this file is associated with.'),
    '#options' => $bundles_select,
    '#default_value' => $file['bid'],
  );

  $form['minify'] = array(
    '#type' => 'checkbox',
    '#title' => t('Minify'),
    '#description' => t('Defines whether the file should be minified using the <a href="@url">selected minification method</a>.', array('@url' => url('admin/build/sf_cache/config/minify'))),
    '#default_value' => $file['minify'],
  );

  $form['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
    '#description' => t('Chooses whether the file is enabled and thus included on pages.'),
    '#default_value' => $file['enabled'],
  );

  $form['cc'] = array(
    '#type' => 'textfield',
    '#title' => t('Conditional Comments'),
    '#description' => t('When filled in, the file will be loaded only in clients which interpret conditional comments and which are matched by this rule. Example: <kbd>"lt IE 7"</kbd>'),
    '#size' => 40,
    '#default_value' => $file['cc'],
  );

  if ($file['bid'] != SF_CACHE_UNBUNDLED) {
    $form['cc']['#disabled'] = TRUE;
    $form['cc']['#description'] .= ' '. t('<strong>Note:</strong> You can\'t specify conditional comments for a file that is contained in a bundle. You can only change the <a href="@change-url">conditional comment</a> of the entire bundle.', array('@change-url' => url('admin/build/sf_cache/bundle/'. $file['bid'], array('query' => drupal_get_destination()))));
  }

  // Store the file ID that is being edited.
  $form['fid'] = array('#type' => 'value', '#value' => $file['fid']);

  $form['buttons'] = array(
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Save file')
    ),
    'delete' => array(
      '#value' => l(t('Delete'), 'admin/build/sf_cache/file/'. $file['fid'] .'/delete', array('query' => drupal_get_destination())),
    ),
    '#weight' => 10,
  );

  return $form;
}


/**
 * Submit callback; Saves the changed information about a file.
 */
function sf_cache_ui_file_edit_submit($form, &$form_state) {
  $file = sf_cache_file_save($form_state['values']);

  if ($file['#updated']) {
    drupal_set_message(t('The file %title has been updated.', array('%title' => $file['filepath'])));
  }
  else {
    drupal_set_message(t('There were no changes to the file %title.', array('%title' => $file['filepath'])));
  }

  $form_state['redirect'] = 'admin/build/sf_cache';
}

/**
 * Menu callback; displays a confirmation page before deleting a file.
 */
function sf_cache_ui_file_delete(&$form_values, $file) {
  $form = array(
    'fid' => array('#type' => 'value', '#value' => $file['fid']),
    'destination' => array('#type' => 'value', '#value' => isset($_GET['destination']) ? $_GET['destination'] : 'admin/build/sf_cache'),
  );

  return confirm_form($form,
     t('Are you sure you want to delete the file %path?', array('%path' => $file['filepath'])),
     isset($_GET['destination']) ? $_GET['destination'] : 'admin/build/sf_cache',
     t('The file will be removed from all bundles in Support File Cache. If it is used again later, it will be added as standalone file. This action cannot be undone.'),
     t('Delete'), t('Cancel'));
}

/**
 * Submit callback; handles removal of a file from the database.
 */
function sf_cache_ui_file_delete_submit($form, &$form_state) {
  $file = sf_cache_file_load($form_state['values']['fid']);
  if ($file) {
    sf_cache_file_delete($form_state['values']['fid']);
    drupal_set_message(t('The file %path has been deleted.', array('%path' => $file['filepath'])));
  }

  // Update all files accordingly.
  sf_cache_ui_submit_update();
}

function theme_sf_cache_ui_datafield($element) {
  return theme('form_element', $element, '<div class="sf-cache-datafield-value">'. $element['#value'] .'</div>');
}
