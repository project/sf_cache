<?php

/**
 * @file
 * Contains the rendering functions for the overview page.
 */

/**
 * Menu callback; shows the overview page with statistics, status report and shortcuts.
 */
function sf_cache_ui_overview() {
  $output = '';

  $statistics_block = array(
    'title' => t('Statistics'),
    'description' => t('Get an overview of your website\'s components.'),
    'content' => sf_cache_ui_overview_statistics(),
  );

  $shortcut_block = array(
    'title' => t('Shortcuts'),
    'description' => t('Update the cache quickly.'),
    'content' => drupal_get_form('sf_cache_ui_overview_shortcuts'),
  );

  $status_report = sf_cache_ui_overview_status_report();
  $status_block = array(
    'title' => t('Status report'),
    'description' => t('View the configuration status of the system.'),
    'content' => theme('status_report', $status_report),
  );

  // Now print everything.
  $output  = '<div class="admin">';
  $output .=   '<div class="clear-block">';
  $output .=     '<div class="left clear-block">';
  $output .=       '<div id="sf-cache-overview-statistics">';
  $output .=         theme('admin_block', $statistics_block);
  $output .=       '</div>';
  $output .=     '</div>';
  $output .=     '<div class="right clear-block">';
  $output .=       theme('admin_block', $shortcut_block);
  $output .=     '</div>';
  $output .=   '</div>';
  $output .=   theme('admin_block', $status_block);
  $output .= '</div>';

  return $output;
}

/**
 * Creates a block of overview statistics for Support File Cache.
 */
function sf_cache_ui_overview_statistics() {
  $statistics = array(
    'css' => array(),
    'js' => array(),
  );

  // Retrieve information about the amount of files and their overall size.
  $size_query = db_query("SELECT kind, COUNT(*) AS files, SUM(size) as size from {sf_cache_file} GROUP BY kind");
  while ($row = db_fetch_array($size_query)) {
    $statistics[$row['kind']] = $row;
  }

  // Retrieve information about the bundles.
  $bundles_query = db_query("SELECT kind, COUNT(*) AS bundles FROM {sf_cache_bundle} GROUP BY kind");
  while ($row = db_fetch_array($bundles_query)) {
    $statistics[$row['kind']] += $row;
  }

  // Create the list with statistics on CSS and JavaScript files.
  $list = array();
  foreach ($statistics as $kind => $numbers) {
    $numbers += array('size' => 0, 'files' => 0, 'bundles' => 0);

    $parameters = array(
      '@size' => format_size($numbers['size']),
      '@kind-url' => url('admin/build/sf_cache/'. $kind),
      '@files' => $numbers['files'],
      '@bundle-url' => url('admin/build/sf_cache/'. $kind .'/by-bundle'),
      '@bundles' => $numbers['bundles'],
    );

    // Note: Plural forms for files are not translatable. Sorry, but that's
    // technically impossible with Drupal's architecture.
    if ($kind == 'css') {
      if ($numbers['bundles'] > 0) {
        $list[] = format_plural($parameters['@bundles'],
          '@size of <a href="@kind-url"><strong>CSS</strong></a> from @files files in <a href="@bundle-url"><strong>1 bundle</strong></a>',
          '@size of <a href="@kind-url"><strong>CSS</strong></a> from @files files in <a href="@bundle-url"><strong>@bundles bundles</strong></a>',
          $parameters
        );
      }
      else {
        $list[] = t('@size of <a href="@kind-url"><strong>CSS</strong></a> from @files files</a>', $parameters);
      }
    }
    else {
      if ($numbers['bundles'] > 0) {
        $list[] = format_plural($parameters['@bundles'],
          '@size of <a href="@kind-url"><strong>JavaScript</strong></a> from @files files in <a href="@bundle-url"><strong>1 bundle</strong></a>',
          '@size of <a href="@kind-url"><strong>JavaScript</strong></a> from @files files in <a href="@bundle-url"><strong>@bundles bundles</strong></a>',
          $parameters
        );
      }
      else {
        $list[] = t('@size of <a href="@kind-url"><strong>JavaScript</strong></a> from @files files</a>', $parameters);
      }
    }
  }

  // Retrieve information about the page statistics.
  // Note: we unite multiple separate queries in one query here.
  $hits_query = db_query(
          "SELECT 'page views' as value, COUNT(DISTINCT access_sec, access_msec) AS amount FROM {sf_cache_file_statistics}
     UNION SELECT 'hits', COUNT(*) FROM {sf_cache_file_statistics}
     UNION SELECT 'kind', COUNT(*) FROM (SELECT f.kind FROM {sf_cache_file_statistics} s JOIN {sf_cache_file} f ON s.fid = f.fid GROUP BY s.fid, f.kind) AS a GROUP BY kind
     UNION SELECT 'earliest', MIN(access_sec) FROM {sf_cache_file_statistics};");

  $values = array();
  while ($row = db_fetch_array($hits_query)) {
    $values[$row['value']] = $row['amount'];
  }

  // Create the statistics fragment on the statistics blog.
  $output = t('Support File Cache module manages...') . theme('item_list', $list);
  $output .= '<p class="info">';
  if ($values['hits'] > 0) {
    $output .= t('It has recorded @hits <strong>hits</strong> on @views <strong>page views</strong> to @css distinct CSS files and @js JavaScript files. The earliest access dates back to @date.', array(
      '@hits' => number_format($values['hits']),
      '@views' => number_format($values['page views']),
      '@css' => $values['css'],
      '@js' => $values['js'],
      '@date' => format_date($values['earliest']),
    ));
  }
  else {
    $output .= t('It did not yet collect any statistical information.');
  }
  $output .= '</p>';

  return $output;
}

/**
 * Form callback; generates the form for the shortcuts on the overview page.
 */
function sf_cache_ui_overview_shortcuts(&$form_state) {
  $form['detect'] = array(
    '#type' => 'submit',
    '#value' => t('Detect files'),
    '#prefix' => '<div class="clear-block">',
    '#suffix' => '<span class="sf-cache-right-arrow">&nbsp;</span>',
    '#submit' => array('sf_cache_ui_config_execute_detect'),
  );

  $form['check'] = array(
    '#type' => 'submit',
    '#value' => t('Find changes'),
    '#suffix' => '<span class="sf-cache-right-arrow">&nbsp;</span>',
    '#submit' => array('sf_cache_ui_config_execute_check'),
  );

  $form['update'] = array(
    '#type' => 'submit',
    '#value' => t('Update bundles'),
    '#suffix' => '</div>',
    '#submit' => array('sf_cache_ui_config_execute_update'),
  );

  $form['info'] = array(
    '#value' => theme('item_list', array(
      t('<strong>Detect files:</strong> Looks for new files and adds them to the database. <em>Does not create or modify files.</em>'),
      t('<strong>Find changes:</strong> Check all existing files for changes and updates the database records. <em>Does not modify files.</em>'),
      t('<strong>Update bundles:</strong> Writes changes to files to disk.'),
    )),
  );

  return $form;
}

/**
 * Creates the status report for the overview page of Support File Cache.
 */
function sf_cache_ui_overview_status_report() {
  // Load .install files
  include_once './includes/install.inc';

  $status = array();
  $enabled = variable_get('sf_cache_enabled', FALSE);

  if ($enabled) {
    $status['status'] = array(
      'severity' => REQUIREMENT_OK,
      'title' => 'Status',
      'value' => t('Enabled'),
    );
  }
  else {
    $status['status'] = array(
      'severity' => REQUIREMENT_WARNING,
      'title' => 'Status',
      'value' => t('Disabled'),
      'description' => t('The replacement of the actual CSS and JavaScript file has been disabled. The original source files from within the module and theme folders will be loaded instead of the processed and bundled version Support File Cache produces. You can <a href="@enable-link">enable</a> the substitution on the <a href="@enable-link">settings page</a>.', array('@enable-link' => url('admin/build/sf_cache/config', array('query' => drupal_get_destination()))))
    );
  }

  if (variable_get('sf_cache_logging', FALSE)) {
    $status['logging'] = array(
      'severity' => REQUIREMENT_INFO,
      'title' => 'Logging',
      'value' => t('Enabled'),
      'description' => t('You can <a href="@disable-link">disable</a> logging if it is not needed to increase the performance.', array('@disable-link' => url('admin/build/sf_cache/config', array('query' => drupal_get_destination()))))
    );
  }
  else {
    $status['logging'] = array(
      'severity' => REQUIREMENT_INFO,
      'title' => 'Logging',
      'value' => t('Disabled'),
      'description' => t('You can <a href="@enable-link">enable</a> logging to collect statistics about how files are used.', array('@enable-link' => url('admin/build/sf_cache/config', array('query' => drupal_get_destination()))))
    );
  }

  $css_directory = file_create_path(variable_get('sf_cache_css_target', 'sf_css'));
  if (!file_check_directory($css_directory, FILE_CREATE_DIRECTORY)) {
    $status['css_target_directory'] = array(
      'severity' => REQUIREMENT_ERROR,
      'title' => t('CSS target directory'),
      'value' => t('Not writable'),
      'description' => t('The CSS target directory %directory does not exist or is not writable. You may need to set the correct directory at the <a href="@sf-cache-settings">settings page</a> or change the current directory\'s permissions so that it is writable.', array('%directory' => $css_directory, '@sf-cache-settings' => url('admin/build/sf_cache/config', array('query' => drupal_get_destination())))),
    );
  }

  $js_directory = file_create_path(variable_get('sf_cache_js_target', 'sf_js'));
  if (!file_check_directory($js_directory, FILE_CREATE_DIRECTORY)) {
    $status['js_target_directory'] = array(
      'severity' => REQUIREMENT_ERROR,
      'title' => t('JavaScript target directory'),
      'value' => t('Not writable'),
      'description' => t('The JavaScript target directory %directory does not exist or is not writable. You may need to set the correct directory at the <a href="@sf-cache-settings">settings page</a> or change the current directory\'s permissions so that it is writable.', array('%directory' => $js_directory, '@sf-cache-settings' => url('admin/build/sf_cache/config', array('query' => drupal_get_destination())))),
    );
  }

  if (variable_get('sf_cache_use_inode', TRUE)) {
    $status['inode'] = array(
      'severity' => REQUIREMENT_INFO,
      'title' => 'Track inode',
      'value' => t('Enabled'),
      'description' => t('Support File Cache uses the <em>inode</em> number and <em>mtime</em> of a file to determine whether it has changed from the last time. This should be <a href="@disable-link">disabled</a> in multi-server environments.', array('@disable-link' => url('admin/build/sf_cache/config', array('query' => drupal_get_destination())))),
    );
  }

  $status['revision'] = array(
    'severity' => REQUIREMENT_INFO,
    'title' => 'Revision number',
    'value' => check_plain(variable_get('sf_cache_revision', '1')),
  );


  global $base_url;

  $css_server = variable_get('sf_cache_css_server', '');
  if (!empty($css_server)) {
    if ($enabled) {
      $status['css_host'] = array(
        'severity' => REQUIREMENT_OK,
        'title' => 'CSS host',
        'value' => 'http://'. check_plain($css_server),
      );
    }
    else {
      $status['css_host'] = array(
        'severity' => REQUIREMENT_WARNING,
        'title' => 'CSS host',
        'value' => $base_url .'/<em>(modules|sites|themes)</em>',
        'description' => t('You have set a CSS host address but Support File Cache is currently disabled. CSS files won\'t be served from the dedicated CSS host.')
      );
    }
  }
  else {
    if ($enabled) {
      $status['css_host'] = array(
        'severity' => REQUIREMENT_INFO,
        'title' => 'CSS host',
        'value' => $base_url .'/'. $css_directory,
      );
    }
    else {
      $status['css_host'] = array(
        'severity' => REQUIREMENT_INFO,
        'title' => 'CSS host',
        'value' => $base_url .'/<em>(modules|sites|themes)</em>',
      );
    }
  }

  $graphics_server = variable_get('sf_cache_graphics_server', '');
  if (!empty($graphics_server)) {
    $status['graphics_host'] = array(
      'severity' => REQUIREMENT_INFO,
      'title' => 'Graphics host',
      'value' => 'http://'. check_plain($graphics_server),
    );
  }

  $js_server = variable_get('sf_cache_js_server', '');
  if (!empty($js_server)) {
    if ($enabled) {
      $status['js_host'] = array(
        'severity' => REQUIREMENT_OK,
        'title' => 'JavaScript host',
        'value' => 'http://'. check_plain($js_server),
      );
    }
    else {
      $status['js_host'] = array(
        'severity' => REQUIREMENT_WARNING,
        'title' => 'JavaScript host',
        'value' => $base_url .'/<em>(modules|sites|themes)</em>',
        'description' => t('You have set a JavaScript host address but Support File Cache is currently disabled. JavaScript files won\'t be served from the dedicated JavaScript host.')
      );
    }
  }
  else {
    if ($enabled) {
      $status['js_host'] = array(
        'severity' => REQUIREMENT_INFO,
        'title' => 'JavaScript host',
        'value' => $base_url .'/'. $js_directory,
      );
    }
    else {
      $status['js_host'] = array(
        'severity' => REQUIREMENT_INFO,
        'title' => 'JavaScript host',
        'value' => $base_url .'/<em>(modules|sites|themes)</em>',
      );
    }
  }

  return $status;
}
