<?php

module_load_include('api.inc', 'sf_cache');

/**
 * @file
 * Contains the menu definitions for Support File Cache module.
 */

$items['admin/build/sf_cache'] = array(
  'title' => 'Support files',
  'description' => t('Intelligently aggregate stylesheets and JavaScript files for faster delivery.'),
  'page callback' => 'sf_cache_ui_overview',
  'file' => 'sf_cache_ui.overview.inc',
  'access arguments' => array('administer Support File Cache'),
);

$items['admin/build/sf_cache/overview'] = array(
  'title' => 'Overview',
  'type' => MENU_DEFAULT_LOCAL_TASK,
  'weight' => -10,
);

{ // admin/build/sf_cache/css
  $items['admin/build/sf_cache/css'] = array(
    'title' => 'CSS',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sf_cache_ui_files_by_bundle', 'css'),
    'file' => 'sf_cache_ui.list.inc',
    'access arguments' => array('administer Support File Cache'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 0,
  );

  $items['admin/build/sf_cache/css/by-bundle'] = array(
    'title' => 'By bundle',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -5,
  );

  $items['admin/build/sf_cache/css/by-path'] = array(
    'title' => 'By path',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sf_cache_ui_files_by_path', 'css'),
    'file' => 'sf_cache_ui.list.inc',
    'access arguments' => array('administer Support File Cache'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 0,
  );

  $items['admin/build/sf_cache/css/autodetect'] = array(
    'title' => 'Autodetect',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sf_cache_ui_autodetect', 'css'),
    'file' => 'sf_cache_ui.list.inc',
    'access arguments' => array('administer Support File Cache'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 5,
  );
}

{ // admin/build/sf_cache/js
  $items['admin/build/sf_cache/js'] = array(
    'title' => 'JavaScript',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sf_cache_ui_files_by_bundle', 'js'),
    'file' => 'sf_cache_ui.list.inc',
    'access arguments' => array('administer Support File Cache'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 5,
  );

  $items['admin/build/sf_cache/js/by-bundle'] = array(
    'title' => 'By bundle',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -5,
  );

  $items['admin/build/sf_cache/js/by-path'] = array(
    'title' => 'By path',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sf_cache_ui_files_by_path', 'js'),
    'file' => 'sf_cache_ui.list.inc',
    'access arguments' => array('administer Support File Cache'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 0,
  );

  $items['admin/build/sf_cache/js/autodetect'] = array(
    'title' => 'Autodetect',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sf_cache_ui_autodetect', 'js'),
    'file' => 'sf_cache_ui.list.inc',
    'access arguments' => array('administer Support File Cache'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 5,
  );
}

{ // admin/build/sf_cache/bundle
  $items['admin/build/sf_cache/bundle'] = array(
    'title' => 'Bundles',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sf_cache_ui_bundle_list'),
    'file' => 'sf_cache_ui.bundle.inc',
    'access arguments' => array('administer Support File Cache'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 10,
  );

  $items['admin/build/sf_cache/bundle/list'] = array(
    'title' => 'List',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -5,
  );

  $items['admin/build/sf_cache/bundle/add'] = array(
    'title' => 'Add',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sf_cache_ui_bundle_edit'),
    'file' => 'sf_cache_ui.bundle.inc',
    'access arguments' => array('administer Support File Cache'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 0,
  );

  $items['admin/build/sf_cache/bundle/delete'] = array(
    'title' => 'Delete empty bundles',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sf_cache_ui_bundles_delete_empty'),
    'file' => 'sf_cache_ui.bundle.inc',
    'access arguments' => array('administer Support File Cache'),
    'type' => MENU_CALLBACK,
  );

  $items['admin/build/sf_cache/bundle/%sf_cache_bundle/edit'] = array(
    'title' => 'Edit',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sf_cache_ui_bundle_edit', 4),
    'file' => 'sf_cache_ui.bundle.inc',
    'access arguments' => array('administer Support File Cache'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 5,
  );

  $items['admin/build/sf_cache/bundle/%sf_cache_bundle/delete'] = array(
    'title' => 'Delete',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sf_cache_ui_bundle_delete', 4),
    'file' => 'sf_cache_ui.bundle.inc',
    'access arguments' => array('administer Support File Cache'),
    'type' => MENU_CALLBACK,
  );
}

{ // admin/build/sf_cache/file

  
  $items['admin/build/sf_cache/file/%sf_cache_file/edit'] = array(
    'title' => 'File',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sf_cache_ui_file_edit', 4),
    'file' => 'sf_cache_ui.file.inc',
    'access arguments' => array('administer Support File Cache'),
    'type' => MENU_CALLBACK,
  );

  $items['admin/build/sf_cache/file/%sf_cache_file/delete'] = array(
    'title' => 'Delete',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sf_cache_ui_file_delete', 4),
    'file' => 'sf_cache_ui.file.inc',
    'access arguments' => array('administer Support File Cache'),
    'type' => MENU_CALLBACK,
  );
}

{ // admin/build/sf_cache/config
  $items['admin/build/sf_cache/config'] = array(
    'title' => 'Configuration',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sf_cache_ui_config_settings'),
    'file' => 'sf_cache_ui.config.inc',
    'access arguments' => array('administer Support File Cache'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 20,
  );

  $items['admin/build/sf_cache/config/settings'] = array(
    'title' => 'Settings',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );

  $items['admin/build/sf_cache/config/paths'] = array(
    'title' => 'Paths',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sf_cache_ui_config_paths'),
    'file' => 'sf_cache_ui.config.inc',
    'access arguments' => array('administer Support File Cache'),
    'type' => MENU_LOCAL_TASK,
    'weight' => -5,
  );

  $items['admin/build/sf_cache/config/maintenance'] = array(
    'title' => 'Maintenance',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sf_cache_ui_config_maintenance'),
    'file' => 'sf_cache_ui.config.inc',
    'access arguments' => array('administer Support File Cache'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 0,
  );

  $items['admin/build/sf_cache/config/excludes'] = array(
    'title' => 'Excludes',
    'page callback' => 'sf_cache_ui_config_excludes',
    'file' => 'sf_cache_ui.config.inc',
    'access arguments' => array('administer Support File Cache'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 5,
  );

  $items['admin/build/sf_cache/config/excludes/delete'] = array(
    'title' => 'Delete exclude',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sf_cache_ui_config_excludes_delete'),
    'file' => 'sf_cache_ui.config.inc',
    'access arguments' => array('administer Support File Cache'),
    'type' => MENU_CALLBACK,
  );

  $items['admin/build/sf_cache/config/minify'] = array(
    'title' => 'Minify',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sf_cache_ui_config_minify'),
    'file' => 'sf_cache_ui.config.inc',
    'access arguments' => array('administer Support File Cache'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 10,
  );
}
