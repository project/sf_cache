
(function($) {
  var SF_CACHE_MINIFICATION_EXTERNAL = 0x0002;

  $(function() {
    $.each([ 'css', 'js' ], function(i, kind) {
      $('input[name="sf_cache_minify_'+ kind +'_mode"]').change(function() {
        $('#sf-cache-'+ kind +'-external')[(this.checked && this.value == SF_CACHE_MINIFICATION_EXTERNAL) ? 'show' : 'hide']();
      }).filter(':checked').change();
    });
  });
})(jQuery);