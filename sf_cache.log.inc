<?php

/**
 * @file
 * Contains functionality for logging the usage of support files.
 */


/**
 * Determines what files are included in this page.
 *
 * @params $variables
 *   Contains the variables that will be available in page.tpl.php.
 */
function sf_cache_log(&$variables) {
  $scopes = variable_get('sf_cache_scopes', array('footer'));
  $file_catalog = sf_cache_file_catalog();

  $hits = array();

  // Loop through the nested CSS hierarchy and add the rows to the $hits array.
  foreach ($variables['css'] as $media => $types) {
    foreach ($types as $type => $files) {
      foreach ($files as $file => $preprocess) {
        // Create a new file record if the file is not yet known.
        if (!isset($file_catalog[$file])) {
          // Include the API since we need functions from it.
          module_load_include('api.inc', 'sf_cache');

          $file_catalog[$file] = sf_cache_file_create('css', $file);
        }

        $hits[] = array(
          'fid' => $file_catalog[$file]['fid'],
          'type' => $type,
          'scope' => $media,
        );
      }
    }
  }

  $js = array('header' => $variables['js']);

  // Collect and save all JS files in all known scopes.
  foreach ($scopes as $scope) {
    $js[$scope] = drupal_add_js(NULL, NULL, $scope);
  }

  // Loop through the nested JS hierarchy and add the rows to the $hits array.
  foreach ($js as $scope => $javascript) {
    foreach ($javascript as $type => $data) {
      // Only log JS files.
      if (in_array($type, array('core', 'module', 'theme'))) {
        foreach ($data as $file => $info) {
          if (!isset($file_catalog[$file])) {
            // Include the API since we need functions from it.
            module_load_include('api.inc', 'sf_cache');

            $file_catalog[$file] = sf_cache_file_create('js', $file);
          }

          $hits[] = array(
            'fid' => $file_catalog[$file]['fid'],
            'type' => $type,
            'scope' => $scope,
          );
        }
      }
    }
  }

  sf_cache_log_hits($hits);
}

/**
 * Records the usage of files.
 *
 * @param $hits
 *   Array of arrays, containg the files. This function bundles the INSERTs for
 *   improved speed. Each hit should have the keys 'fid', 'type' and 'scope'.
 */
function sf_cache_log_hits($hits) {
  $request = $_GET['q'];

  $access = microtime(TRUE);
  $access_msec = (int)(($access - (int)$access) * 1000000);

  // Start to construct the SQL query.
  $query = "INSERT DELAYED INTO {sf_cache_file_statistics} (access_sec, access_msec, fid, request, type, scope) VALUES ";
  $parameters = array();

  // Add all hits to one query.
  foreach ($hits as $id => $hit) {
    $hits[$id] = "(%d, %d, %d, '%s', '%s', '%s')";
    array_push($parameters, $access, $access_msec, $hit['fid'], $request, $hit['type'], $hit['scope']);
  }

  $query .= implode(', ', $hits);

  db_query($query, $parameters);
}
