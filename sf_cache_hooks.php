<?php

/**
 * @file
 * These are the hooks available for sf_cache.
 *
 * When a file is changed, a bundle will be reassembled unless file_save() is
 * told not to do so.
 *
 * Whenever something changes about an entity controlled by sf_cache, caches
 * related to this module will be cleared if not specified differently.
 *
 * Support File Cache deals with three entity types:
 * - file: A single CSS or JavaScript file
 * - bundle: A group of files
 * - composite: A processed file or bundle
 */


/**
 * Variables:
 *
 * - sf_cache_enabled
 *     Boolean; If true, files will be replaced with their bundles. Defaults to FALSE.
 * - sf_cache_enabled_themes
 *     Array of themes the module should be enabled for. Defaults to an empty array().
 * - sf_cache_logging
 *     Boolean; Controls logging of hits. Defaults to FALSE.
 * - sf_cache_scopes (NOT USED)
 *     Array of all scopes used on the site, except for 'header'. Defaults to array('footer').
 * - sf_cache_writable (NOT USED)
 *     Boolean; Controls the write mode. TRUE if the module may write.
 * - sf_cache_autoupdate
 *     Boolean; If true, updates will be written when they are due.
 * - sf_cache_use_inode
 *     Boolean; Defines whether the inode and mtime of a file are used to calc the hash.
 * - sf_cache_security_hash
 *     A random hash that is used to protect access to the GET api URLs.
 * - sf_cache_constant_config_hash
 *     If not empty, this string will be used as base for the configuration hash.
 * - sf_cache_excludes
 *     An array containing all paths to be excluded from treatment by sf_cache.
 *
 * - sf_cache_css_target
 *     Directory in the files directory in which CSS files will end up bundled. Defaults to 'css'.
 * - sf_cache_css_source
 *     The base directory of the location CSS files are stored at. Defaults to ''.
 * - sf_cache_css_server
 *     The URL prefix of the CSS server.
 *
 * - sf_cache_js_target
 *     Directory in the files directory in which JS files will end up bundled. Defaults to 'js'.
 * - sf_cache_js_source
 *     The base directory of the location JS files are stored at. Defaults to ''.
 * - sf_cache_js_server
 *     The URL prefix of the JavaScript server.
 *
 * - sf_cache_graphics_server
 *     The URL prefix where image files are hosted.
 * - sf_cache_graphics_revision
 *     The suffix that will be appended to graphics.
 *
 * - sf_cache_minify_css_mode
 *     The minification mode for CSS files (None, Built-In, External). Defaults
 *     to the built-in mechanism.
 * - sf_cache_minify_css_executable
 *     The path to a third-party minification application for CSS.
 * - sf_cache_minify_css_parameters
 *     The parameters for the minification application.
 * - sf_cache_minify_js_mode
 *     The minification mode for JavaScript files (None, External). Defaults to None.
 * - sf_cache_minify_js_executable
 *     The path to a third-party minification application for JavaScript.
 * - sf_cache_minify_js_parameters
 *     The parameters for the minification application.
 */


/**
 * @addtogroup hooks
 * @{
 */

/**
 * Bundle hooks.
 */

/**
 * Act upon the creation of a bundle.
 *
 * Called after a new bundle has been inserted into the database.
 */
function hook_sfc_bundle_create($bundle) {
  // No sample implementation yet.
}

/**
 * Act upon the update of a bundle.
 *
 * Properties of the bundle have been changed (e.g. the title). NOTE: This
 * hook is *not* called when files are added to or removed from this bundle.
 */
function hook_sfc_bundle_update($bundle, $old_bundle) {
  // No sample implementation yet.
}

/**
 * Act upon the deletion of a bundle.
 *
 * A bundle is being deleted. Use this to do cleanup tasks. NOTE: You don't
 * have to remove the association to this bundle from files as that's auto-
 * matically dealt with.
 */
function hook_sfc_bundle_delete($bundle) {
  // No sample implementation yet.
}


/**
 * File hooks.
 */

/**
 * Act upon the creation of a file.
 *
 * A new file is added to the database. It does not have a bundle association
 * by default.
 */
function hook_sfc_file_create($file) {
  // No sample implementation yet.
}

/**
 * Act upon the update of a file.
 *
 * Properties of a file are updated (e.g. the actual content of a file,
 * the bundle affiliation). NOTE: do not recreate a bundle as a result of
 * an invocation of this hook. These will be separate bundle hook calls.
 */
function hook_sfc_file_update($file, $old_file) {
  // No sample implementation yet.
}

/**
 * Act upon the deletion of a file record.
 *
 * A file is removed from the database (it has already been removed from the
 * bundle automatically beforehand). NOTE: Support File Cache does not physically
 * delete files. This only refers to the deletion of the database record.
 */
function hook_sfc_file_delete($file) {
  // No sample implementation yet.
}


/**
 * Allows modification of a file's content before it is added to the composite.
 *
 * Fired when a file is about to be added to a bundle (physically during
 * assembly). Can be used to modify the file's content and change paths.
 */
function hook_sfc_file_modify($file, $bundle, $filepath, &$content) {
  // No sample implementation yet.
}


/**
 * Composite hooks.
 */

/**
 * Allows deployment of a bundle to another location.
 *
 * Called after a file has been assembled and is ready for deployment
 * on a CDN (for example). $obj is either a bundle or a file array.
 */
function hook_sfc_composite_deploy($obj, $filepath) {
  // No sample implementation yet.
}

/**
 * Allows removal of a deployed composite file.
 *
 * Called when a physical file is about to be removed from the server. Can
 * be used to delete the file from the CDN as well.
 */
function hook_sfc_composite_retract($obj, $filepath) {
  // No sample implementation yet.
}

/**
 * Allows alterations to the composite file process.
 *
 * Can be used to add/remove files from the bundle before it is being
 * created. The filepath ($processed) can also be changed.
 */
function hook_sfc_composite_alter($obj, &$filepath, &$files) {
  // No sample implementation yet.
}

/**
 * Allows modification of the composite file's content.
 *
 * Modify the contents of the overall bundle (e.g. rewrite paths, minify);
 * Called during the assembly of a bundle and before it is saved.
 */
function hook_sfc_composite_modify($obj, $filepath, $files, &$content) {
  // No sample implementation yet.
}

/**
 * @} End of "addtogroup hooks".
 */