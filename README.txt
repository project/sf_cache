
SYNOPSIS
--------
Support File Cache is a module that allows more intelligent bundling of CSS
and JavaScript files by grouping only files that belong together. While the
aggregation mechanism in the Drupal core creates a new "bundle" for each set
of files, Support File Cache allows you to create for example one "main"
bundle that is used throughout your site and additional bundles that are only
used on certain pages. It also adds features to control the delivery of these
files to the user by allowing to change the hosts from which they are served.



INSTALLATION AND CONFIGURATION
------------------------------
Please see INSTALL.txt.



SETUP
-----
Support File Cache will create two directories, titled "sf_css" and "sf_js".
These directories contain the processed ("composite") files created by this
module.

Support File Cache will scan your Drupal installation for CSS and JavaScript
files and add them to its database tables. You can now create bundles using
the administration area. All of Support File Cache's admin pages are located
at Administer >> Site building >> Support files. You can either create bundles
manually or use the autodetection feature. This feature works best after it
has collected some data. You can turn on logging on the Configuration page.
Note that results from the autodetection are not perfect, especially on bigger
sites.

For a simple site on one server, you can stop here.



CONFIGURATION
-------------

If you want to serve your support files from another server, you need a way to
copy them to that server once Support File Cache creates them. You can either
establish a NFS connection to that server, mounted at the directory you
specified on the Configuration page or write a small custom module
implementing the composite_deploy and composite_retract hooks from this
module. This allows you to automatically push changes to that server. You can
also set up a transparent proxy which fetches the files from the www server.
This also applies to content delivery networks.

It is also possible to add a third-party minification application like the YUI
Compressor. Navigate to the Config >> Minification page, select "External" for
the file type you want to minify. Specify the command to use for executing the
minifier and supply any arguments you want. E.g. if you have the YUI
compressor installed in "sites/all/modules/sf_cache/bin", the executable is:

    java -jar sites/all/modules/sf_cache/bin/yuicompressor-2.3.5.jar

As parameters for JavaScript minification, it is advised to use the --nomunge
parameter. For additional parameters, consult the documentation of the third
party minifier application you use.

Have fun.



TROUBLESHOOTING
---------------

It is possible that you get a "The Support File Cache module can't hook into
your theme. Please consult the manual." message depending on the theme you
use. To solve this problem, you unfortunately have to modify your theme. Copy
the phptemplate_closure() function found in sf_cache.module's source code and
integrate it into your theme's phptemplate_closure() function or
<themename>_closure() function.