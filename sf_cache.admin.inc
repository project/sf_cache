<?php

/**
 * @file
 * Provides administrative functions which are not needed on every page view.
 */


// Include the API since we need functions from it.
module_load_include('api.inc', 'sf_cache');

/**
 * Recalculates the tables which are need for suggesting bundles.
 */
function sf_cache_bundle_reset_temp() {
  // Calculate the number of requests for each file.
  db_query("DELETE FROM {sf_cache_temp_counter}");
  db_query("INSERT INTO {sf_cache_temp_counter}
  SELECT f.fid, f.filepath, f.kind, f.size, COUNT(*) as total
    FROM {sf_cache_file} f FORCE INDEX (PRIMARY) JOIN {sf_cache_file_statistics} s WHERE (f.fid = s.fid)
    GROUP BY f.fid ORDER BY NULL");

  // Calculate the number of requests two files have been used together.
  db_query("DELETE FROM {sf_cache_temp_relation}");
  db_query("INSERT INTO {sf_cache_temp_relation}
  SELECT s1.fid as fid1, s2.fid as fid2, COUNT(*) as quantity
    FROM {sf_cache_file_statistics} s1 JOIN {sf_cache_file_statistics} s2 ON (s1.fid <= s2.fid AND s1.access_sec = s2.access_sec AND s1.access_msec = s2.access_msec)
    GROUP BY s1.fid, s2.fid ORDER BY NULL");
}

/**
 * Determines bundles based on the frequency they are used together.
 *
 * @param $threshold
 *   A number that is used as a threshold when generating equivalence classes
 *   of bundles. Ranges from 1 to 100.
 * @param $kind
 *   The kind of files bundles should be generated for.
 * @param $reset
 *   Whether to reset the helper tables. If you generate several suggestions,
 *   use TRUE for the first suggestion and FALSE on subsequent calls.
 * @return
 *   An array of files that are in one bundle.
 */
function sf_cache_bundle_suggest($threshold = 20, $kind = NULL, $reset = FALSE) {
  if ($reset) {
    sf_cache_bundle_reset_temp();
  }

  // NOTE: 8192 is a fixed size in bytes
  $query = "SELECT c1.fid AS fid1, c1.kind, c1.filepath AS path1, c2.fid AS fid2, c2.filepath AS path2
    FROM ({sf_cache_temp_counter} c1 FORCE INDEX (PRIMARY) JOIN {sf_cache_temp_counter} c2 ON (c1.kind = c2.kind AND c1.fid <= c2.fid))
      JOIN {sf_cache_temp_relation} r ON (c1.fid = r.fid1 AND c2.fid = r.fid2)
    WHERE ROUND((r.quantity / c1.total) * (r.quantity / c2.total) * (SQRT(8192 / c1.size) * SQRT(8192 / c2.size)) * 100) >= %d";

  // Add in the kind selection, if present.
  if (isset($kind)) {
    $query .= " AND c1.kind = '%s'";
  }

  $query .= " ORDER BY c1.fid, c2.fid";

  // Retrieve all files from one equivalence class.
  $result = db_query($query, $threshold, $kind);

  // Calculate the equivalence class for the files.
  $classes = array();
  while ($row = db_fetch_array($result)) {
    if (!isset($classes[$row['kind']])) {
      $classes[$row['kind']] = array();
    }

    // Create a new equivalence class if it doesn't exist yet.
    if (!isset($classes[$row['kind']][$row['fid1']])) {
      $classes[$row['kind']][$row['fid1']] = array($row['fid1'] => $row['path1']);
    }

    // Link the second file to the equivalence class.
    $classes[$row['kind']][$row['fid1']][$row['fid2']] = $row['path2'];
    $classes[$row['kind']][$row['fid2']] = &$classes[$row['kind']][$row['fid1']];
  }

  foreach (array_keys($classes) as $kind) {
    $classes[$kind][SF_CACHE_UNBUNDLED] = array();

    // (Use while/list/each because we modify the array we iterate over, so foreach fails.)
    while (list($id, $class) = each($classes[$kind])) {
      // Merge bundles with only one item.
      if ($id != SF_CACHE_UNBUNDLED && count($class) == 1) {
        $classes[$kind][SF_CACHE_UNBUNDLED] += $class;
        unset($classes[$kind][$id]);
        continue;
      }

      // Deletes duplicate equivalence classes, uses the first element as defining element.
      foreach (array_keys($class) as $key) {
        if ($key != $id) {
          unset($classes[$kind][$key]);
        }
      }
    }
  }

  return $classes;
}

/**
 * Searches for new support files and adds them to the database.
 */
function sf_cache_detect_files() {
  // Clear the cache.
  sf_cache_clear_cache();

  // Check and update old files first.
  sf_cache_check_files(FALSE);

  $files = sf_cache_file_catalog(TRUE);
  $result = array();

  foreach (array('css', 'js') as $kind) {
    $new = array_keys(_sf_cache_file_listing($kind));
    foreach ($new as $path) {
      if (!isset($files[$path])) {
        if (sf_cache_file_create($kind, $path)) {
          $result[] = check_plain($path);
        }
      }
    }
  }

  return $result;
}

/**
 * Returns a list of certain files in certain directories.
 *
 * @param $kind
 *   The type of files to look for.
 * @return
 *   An array of arrays containing information on the found files.
 */
function _sf_cache_file_listing($kind) {
  global $conf;
  $config = conf_path();
  $prefix = variable_get('sf_cache_'. $kind .'_source', '');

  if ($prefix == '.') $prefix = '';
  if (!empty($prefix)) $prefix .= '/';

  $prefix_length = strlen($prefix);

  $search_dirs =  array(
    $prefix .'misc',
    $prefix .'modules',
    $prefix .'themes',
    $prefix .'sites/all/modules',
    $prefix .'sites/all/themes',
    $prefix .'profiles/'. $conf['install_profile'] .'/modules',
    $prefix .'profiles/'. $conf['install_profile'] .'/themes',
    $prefix . $config .'/modules',
    $prefix . $config .'/themes',
  );

  $files = array();
  foreach ($search_dirs as $dir) {
    $result = file_scan_directory($dir, '\.'. $kind .'$');
    foreach ($result as $file) {
      // Remove the prefix.
      $file->filename = substr($file->filename, $prefix_length);
      $files[$file->filename] = $file;
    }
  }

  return $files;
}

/**
 * Checks support files for changes (e.g. used by hook_cron()).
 *
 * @param $update
 *   (optional) Updates composite files afterwards.
 */
function sf_cache_check_files($update = TRUE) {
  clearstatcache();

  $files = sf_cache_file_load();
  $changes = array();

  foreach (array('css', 'js') as $kind) {
    foreach ($files[$kind] as $file) {
      $info = sf_cache_file_info($file['filepath'], $kind);

      if ((!$info && !empty($file['hash'])) || ($info['hash'] != $file['hash'])) {
        // Save the file with an empty hash to indicate it's not there anymore.
        sf_cache_file_save($file, $info, FALSE);
        $changes[] = check_plain($file['filepath']);
      }
    }
  }

  if ($update && !empty($changes)) {
    // Setting $check to FALSE prevents loop.
    sf_cache_update_composites(FALSE);
  }

  return $changes;
}

/**
 * Update composite files.
 *
 * @param $check
 *   (optional) If set to TRUE, files are checked for modifications of their
 *   content prior to generating new bundles.
 * @param $force
 *   (optional) Forces an update even if autoupdating is disabled.
 * @return
 *   An array of files that have been altered.
 */
function sf_cache_update_composites($check = TRUE, $force = FALSE) {
  if (!variable_get('sf_cache_autoupdate', TRUE) && !$force) {
    return;
  }

  if ($check) {
    sf_cache_check_files(FALSE);
  }

  // Create bundles.
  $bundle_categories = sf_cache_bundle_load();
  $changes = array();

  foreach ($bundle_categories as $kind => $bundles) {
    foreach ($bundles as $bundle) {
      $changes[] = sf_cache_build_bundle($bundle, FALSE);
    }
  }

  // Create a processed file for each file.
  $files = sf_cache_file_load('filepath');
  $files = $files['css'] + $files['js'];
  foreach ($files as $fid => $file) {
    if (empty($file['hash'])) {
      // Delete the processed version.
      $file['processed'] = '';
      sf_cache_file_save($file, NULL, FALSE);
    }
    else {
      // Create or update the processed version.
      $changes[] = sf_cache_build_file($file, FALSE);
    }
  }

  sf_cache_clear_cache();

  // Remove empty values from the array of changed files.
  foreach ($changes as $key => $path) {
    if (empty($path)) {
      unset($changes[$key]);
    }
  }

  return $changes;
}

/**
 * Creates the processed file for a bundle.
 *
 * @param $bundle
 *   A bundle array containing information about the bundle to generate.
 * @param $clear
 *   (optional) If set to TRUE (default), the cache will be cleared afterwards.
 * @return
 *   An array of the files changed/processed.
 */
function sf_cache_build_bundle($bundle, $clear = TRUE) {
  // Get all files from this bundle.
  $files = array();
  $hash = sf_cache_config_hash() .':'. (int)$bundle['minify'] .':';
  $result = db_query("SELECT filepath, hash FROM {sf_cache_file} WHERE bid = %d AND hash <> '' AND kind = '%s' AND enabled <> 0 ORDER BY weight ASC, fid ASC", $bundle['bid'], $bundle['kind']);

  $changed = NULL;

  while ($file = db_fetch_array($result)) {
    $files[] = $file['filepath'];
    $hash .= $file['hash'];
  }

  // Generate filename.
  $directory = file_create_path(variable_get('sf_cache_'. $bundle['kind'] .'_target', 'sf_'. $bundle['kind']));
  file_check_directory($directory, FILE_CREATE_DIRECTORY);

  // Create the bundle's filename.
  $name = drupal_strtolower(preg_replace('/\W+/', '_', $bundle['title']));
  $processed = $directory .'/'. $name .'_'. substr(md5($hash), 0, 12) .'_'. variable_get('sf_cache_revision', '1') .'.'. $bundle['kind'];

  // Create the file if it does not yet exist or if the file contents have changed.
  if ($processed != $bundle['processed'] || !file_exists($processed)) {
    foreach (module_implements('sfc_composite_alter') as $module) {
      $function = $module .'_sfc_composite_alter';
      $function($bundle, $processed, $files);
    }

    // Call the right assembling function.
    if ($bundle['kind'] == 'css') {
      sf_cache_assemble_css('bundle', $bundle, $processed, $files);
    }
    elseif ($bundle['kind'] == 'js') {
      sf_cache_assemble_js('bundle', $bundle, $processed, $files);
    }
    else {
      return;
    }

    $bundle['processed'] = $processed;
    $bundle = sf_cache_bundle_save($bundle, FALSE);
    module_invoke_all('sfc_composite_deploy', $bundle, $processed);

    // Store the filepath of the new bundle for display in info boxes.
    $changed = check_plain($processed);
  }

  if ($clear) {
    sf_cache_clear_cache();
  }

  return $changed;
}

/**
 * Creates a processed version of a file.
 *
 * @param $file
 *   A file array containing information about the file to process.
 * @param $clear
 *   (optional) If set to TRUE (default), the cache will be cleared afterwards.
 * @param $save
 *   (optional) If TRUE, the new processed file name is saved to the database.
 * @return
 *   A string with the processed filename and a hint to the original file).
 */
function sf_cache_build_file($file, $clear = TRUE, $save = TRUE) {
  // Get all files from this bundle.
  $files = array($file['filepath']);

  $changed = NULL;

  // Generate filename.
  $directory = file_create_path(variable_get('sf_cache_'. $file['kind'] .'_target', 'sf_'. $file['kind']));
  file_check_directory($directory, FILE_CREATE_DIRECTORY);

  $processed = $directory .'/'. preg_replace('/[^\\w]+/', '_', basename($file['filepath'], '.'. $file['kind'])) .'_'. substr(md5(sf_cache_config_hash() .':'. (int)$file['minify'] .':'. $file['hash']), 0, 12) .'_'. variable_get('sf_cache_revision', '1') .'.'. $file['kind'];

  // Create the file if it does not yet exist or if the file contents have changed.
  if ($processed != $file['processed'] || !file_exists($processed)) {
    foreach (module_implements('sfc_composite_alter') as $module) {
      $function = $module .'_sfc_composite_alter';
      $function($file, $processed, $files);
    }

    // Call the right assembling function.
    if ($file['kind'] == 'css') {
      sf_cache_assemble_css('file', $file, $processed, $files, TRUE);
    }
    elseif ($file['kind'] == 'js') {
      sf_cache_assemble_js('file', $file, $processed, $files);
    }
    else {
      return;
    }

    $file['processed'] = $processed;

    if ($save) {
      $file = sf_cache_file_save($file, NULL, FALSE);
    }

    module_invoke_all('sfc_composite_deploy', $file, $processed);

    // Store the filepath of the processed file for display in info boxes.
    $changed = t('<a href="@filepath-url">@filepath</a> (created from <a href="@original-url">@original</a>)',
      array(
        '@filepath-url' => base_path() . $processed,
        '@filepath' => $processed,
        '@original-url' => base_path() . $file['filepath'],
        '@original' => $file['filepath'],
      ));
  }

  if ($clear) {
    sf_cache_clear_cache();
  }

  return $changed;
}

/**
 * Aggregate and optimize CSS files, putting them in the files directory.
 *
 * @param $type
 *  The type of composite file that is build (bundle or standalone file).
 * @param $bundle
 *  The bundle (or file) array.
 * @param $processed
 *   The path for the final bundle.
 * @param $files
 *   An array with CSS files belonging to that bundle.
 */
function sf_cache_assemble_css($type, &$bundle, &$processed, $files, $import = TRUE) {
  $contents = '';
  foreach ($files as $file) {
    $path = sf_cache_file_path($file, 'css');
    $data = sf_cache_load_stylesheet($path, $import);

    // Return the path to where this CSS file originated from.
    $base = base_path() . dirname($file) .'/';
    _sf_cache_build_css_path(NULL, $base);

    // Prefix all paths within this CSS file.
    $data = preg_replace_callback('~
      url\\(\\s*                # Spec only allows whitespace after "url(".
        ([\'"])?                # Optionally match a quote into \1
        (?(1)                   # If there is something in \1 ...
                                # ... do nothing.
          |                     # else ...
          (?![\'"])             # ... make sure there is really no quote.
        )                       #
        ((?:                    # \2 opens here
          (?(1)                 # If there is something in \1 ...
            (?!(?:\\\\|\\1)).   # ... match anything that is not a slash or \1.
            |                   # else ...
            [^\\)]              # ... match anything but a closing bracket.
          )                     #
          |                     # or
          \\\\.                 # match an escaped character
        )+)                     # matches >= 1 of these characters
        (?(1)                   # PHPs PCRE does not allow just writing \1
          \\1                   # add the closing quote
          |                     # or
                                # nothing
        )                       #
      \\s*\\)                   # allow whitespace after the quote
    ~six', '_sf_cache_build_css_path', $data);


    foreach (module_implements('sfc_file_modify') as $module) {
      $function = $module .'_sfc_file_modify';
      $function($file, $bundle, $processed, $data);
    }

    $contents .= $data;
  }

  if ($type == 'bundle') {
    $bundle['size'] = strlen($contents);
  }

  foreach (module_implements('sfc_composite_modify') as $module) {
    $function = $module .'_sfc_composite_modify';
    $function($bundle, $processed, $files, $contents);
  }

  // Create the CSS file.
  file_save_data($contents, $processed, FILE_EXISTS_REPLACE);
}

/**
 * Helper function for sf_cache_assemble_css().
 *
 * This function will prefix all paths within a CSS file according to the setting
 * of Support File Cache.
 */
function _sf_cache_build_css_path($matches, $base = NULL) {
  global $base_url;
  static $_base, $_file, $css = NULL, $graphics = NULL;
  // Store base path for preg_replace_callback.
  if (isset($base)) {
    $_base = $base;
  }

  // Load information about graphic paths.
  if ($css == NULL || $graphics == NULL) {
    $css = array();
    
    $_css = explode('/', variable_get('sf_cache_css_server', ''), 2);
    if (!empty($css[1])) {
      list($css['host'], $css['path']) = $_css;
    }
    else {
      $css = parse_url($base_url);
    }

    $graphics = array(
      'rev_mode' => variable_get('sf_cache_graphics_revision', SF_CACHE_GRAPHICS_REV_NONE),
      'rev' => variable_get('sf_cache_revision', '1'),
    );

    $_graphics = explode('/', variable_get('sf_cache_graphics_server', ''), 2);
    if (!empty($_graphics[1])) {
      list($graphics['host'], $graphics['path']) = $_graphics;
    }
    else {
      $graphics = array_merge($graphics, parse_url($base_url));
      $graphics['path'] = '';
    }

    if (!empty($graphics['path']) && $graphics['path'][0] != '/') {
      $graphics['path'] = '/'. $graphics['path'];
    }
  }

  if (preg_match('~^\w+:~i', $matches[2])) {
    // Do not touch absolute URLs
    $path = $matches[2];
  }
  else {
    // Prepend the host root relative path to this CSS file if it's not
    // an absolute path.
    if ($matches[2][0] != '/') {
      $matches[2] = $_base . $matches[2];
    }

    // Prefix the path appropriately.
    $path = $graphics['path'] . $matches[2];

    // Remove "./".
    $path = preg_replace('~(^|/)\./~', '$1', $path);

    // If the graphic files are not on the css host, make it an absolute URL.
    if ($graphics['host'] != $css['host']) {
      $path = 'http://'. $graphics['host'] . $path;
    }

    $last = "\0";
    // Remove 'folder/../' segments where possible.
    while ($path != $last) {
      $last = $path;
      $path = preg_replace('~(^|/)(?!\.\./)([^/]+)/\.\./~', '$1', $path);
    }

    if ($graphics['rev_mode'] == SF_CACHE_GRAPHICS_REV_QUERY) {
      // Append the file revision as query.
      $path .= ((strpos($path, '?') !== FALSE) ? '&' : '?') .'rev='. $graphics['rev'];
    }
    elseif ($graphics['rev_mode'] == SF_CACHE_GRAPHICS_REV_FILENAME) {
      // Insert the revision number just before the file extension.
      $path = substr_replace($path, '_r'. $graphics['rev'], strrpos($path, '.'), 0);
    }
  }

  // If no quotes were matched, at least define the index so that we don't get
  // notices.
  if (!isset($matches[1])) {
    $matches[1] = '';
  }

  // Optionally add the quotes back
  return 'url('. $matches[1] . $path . $matches[1] .')';
}

/**
 * Loads the stylesheet and resolves all @import commands. (Copied from Drupal 6)
 *
 * Loads a stylesheet and replaces @import commands with the contents of the
 * imported file. Use this instead of file_get_contents when processing
 * stylesheets.
 *
 * The returned contents are minified removing white space and comments only
 * when CSS aggregation is enabled. This optimization will not apply for
 * color.module enabled themes with CSS aggregation turned off.
 *
 * @param $file
 *   Name of the stylesheet to be processed.
 * @param $import
 *   Determins whether @import files should be loaded into the bundle.
 * @return
 *   Contents of the stylesheet including the imported stylesheets.
 */
function sf_cache_load_stylesheet($file, $import = TRUE) {
  $contents = '';
  if (file_exists($file)) {
    // Load the local CSS stylesheet.
    $contents = file_get_contents($file);

    // Change to the current stylesheet's directory.
    $cwd = getcwd();
    chdir(dirname($file));

    if ($import) {
      // Replaces @import commands with the actual stylesheet content.
      // This happens recursively but omits external files.
      $contents = preg_replace_callback('/@import\s*(?:url\()?[\'"]?(?![a-z]+:)([^\'"\()]+)[\'"]?\)?;/', '_sf_cache_load_stylesheet', $contents);
    }

    // Remove multiple charset declarations for standards compliance (and fixing Safari problems).
    $contents = preg_replace('/^@charset\s+[\'"](\S*)\b[\'"];/i', '', $contents);

    // Change back directory.
    chdir($cwd);
  }

  return $contents;
}

/**
 * Loads stylesheets recursively and returns contents with corrected paths. (Copied from Drupal 6)
 *
 * This function is used for recursive loading of stylesheets and
 * returns the stylesheet content with all url() paths corrected.
 */
function _sf_cache_load_stylesheet($matches) {
  $filename = $matches[1];
  // Load the imported stylesheet and replace @import commands in there as well.
  $file = sf_cache_load_stylesheet($filename);

  // Determine the correct directory (we don't need . as current directory).
  $directory = dirname($filename) == '.' ? '' : dirname($filename) .'/';

  // Alter all url() paths, but not external. We don't need to normalize
  // paths here (remove folder/... segements) because that will be done later.
  return preg_replace('/url\s*\(([\'"]?)(?![a-z]+:|\/+|[\'"])/i', 'url(\1'. $directory, $file);
}

/**
 * Aggregate JS files, putting them in the files directory.
 *
 * @param $type
 *  The type of composite file that is build (bundle or standalone file).
 * @param $bundle
 *  The bundle (or file) array.
 * @param $processed
 *   The path for the final bundle.
 * @param $files
 *   An array with JS files belonging to that bundle.
 */
function sf_cache_assemble_js($type, &$bundle, &$processed, $files) {
  $contents = '';

  foreach ($files as $file) {
    $file = sf_cache_file_path($file, 'js');
    // Append a ';' after each JS file to prevent them from running together.
    $data = file_get_contents($file) .';';

    foreach (module_implements('sfc_file_modify') as $module) {
      $function = $module .'_sfc_file_modify';
      $function($file, $bundle, $processed, $data);
    }

    $contents .= $data;
  }

  if ($type == 'bundle') {
    $bundle['size'] = strlen($contents);
  }

  foreach (module_implements('sfc_composite_modify') as $module) {
    $function = $module .'_sfc_composite_modify';
    $function($bundle, $processed, $files, $contents);
  }

  // Create the JS file.
  file_save_data($contents, $processed, FILE_EXISTS_REPLACE);
}

/**
 * Implementation of hook_sfc_composite_modify().
 */
function sf_cache_sfc_composite_modify($obj, $filepath, $files, &$content) {
  static $mode = NULL;

  // Restart the time limit counter.
  @set_time_limit(30);

  // Fetch the minification modes.
  if ($mode === NULL) {
    $mode = array(
      'css' => variable_get('sf_cache_minify_css_mode', SF_CACHE_MINIFICATION_BUILTIN),
      'js' => variable_get('sf_cache_minify_js_mode', SF_CACHE_MINIFICATION_DISABLED),
    );
  }

  // Only minify if it is enabled.
  if ($obj['minify']) {
    switch ($mode[$obj['kind']]) {
      case SF_CACHE_MINIFICATION_BUILTIN:
        // Perform safe built-in file shrinking optimizations.
        $content = preg_replace('<
          \s*([@{}:;,]|\)\s|\s\()\s* |  # Remove whitespace around separators, but keep space around parentheses.
          /\*([^*\\\\]|\*(?!/))+\*/ |   # Remove comments that are not CSS hacks.
          [\n\r]                        # Remove line breaks.
          >x', '\1', $content);
        break;
      case SF_CACHE_MINIFICATION_JSMIN:
        if ($obj['kind'] == 'js' && sf_cache_jsmin_available()) {
          try {
            $content = JSMin::minify($content);
          }
          catch (JSMinException $e) {
            watchdog('sf_cache', 'File minification for %file failed. JSMin threw an exception with the following message: <pre>@return</pre>', array('%file' => $filepath, '@return' => $e->getMessage()), WATCHDOG_WARNING);
          }
        }
        break;
      case SF_CACHE_MINIFICATION_EXTERNAL:
        // Invoke a third-party minifier.
        $content = sf_cache_minify($obj['kind'], $content);
        break;
    }
  }
}

/**
 * Tries to find and include JSMin.
 *
 * @return
 *   TRUE when JSMin is avilable, FALSE otherwise. If JSMin is available,
 *   it's automatically included.
 */
function sf_cache_jsmin_available() {
  $files = file_scan_directory(drupal_get_path('module', 'sf_cache'), '^jsmin-.+\.php$');

  if (!empty($files)) {
    include_once './'. key($files);
    return class_exists('JSMin');
  }
  else {
    return FALSE;
  }
}

/**
 * Minify a support file.
 *
 * @param $kind
 *   The kind of file to minify ('css' or 'js').
 * @param $content
 *   The contents of the file.
 * @return
 *   The minified result or the original content if minification failed.
 */
function sf_cache_minify($kind, $content) {
  // Save the file so that it can be processed by the minifier.
  $temp_path = file_directory_temp() . md5(microtime()) .'.'. $kind;
  $temp_path = file_save_data($content, $temp_path);

  if (!empty($temp_path)) {
    $exec = variable_get('sf_cache_minify_'. $kind .'_executable', '');
    $params = variable_get('sf_cache_minify_'. $kind .'_parameters', '');

    // Only run the external app when there is a path specified.
    if (!empty($exec)) {
      // Prepare variables for capturing the status.
      $status = NULL;
      $return = NULL;

      set_time_limit(30);

      // Prepare the command.
      $cmd = escapeshellcmd($exec .' '. preg_replace('/\\r|\\n/', ' ', $params) .' '. $temp_path);

      // Execute the external minifier.
      $processed = exec($cmd, $return, $status);

      if ($status !== 0) {
        watchdog('sf_cache', 'File minification for %file failed.

The following command has been executed:
<pre>@command</pre>

It returned the following:
<pre>@return</pre>', array('%file' => $processed, '@command' => $cmd, '@return' => var_export($return, TRUE)), WATCHDOG_WARNING);
        unset($processed);
      }
    }

    // Remove the temporarily created file.
    file_delete($temp_path);
  }

  return (isset($processed))? $processed : $content;
}
