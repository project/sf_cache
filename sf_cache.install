<?php

/**
 * @file
 * Database scheme for Support File Cache.
 */

/**
 * Implementation of hook_schema().
 */
function sf_cache_schema() {
  $schema['sf_cache_bundle'] = array(
    'description' => 'Stores the available bundle and associated information.',
    'fields' => array(
      'bid' => array(
        'description' => 'Unique bundle ID.',
        'type' => 'serial',
        'not null' => TRUE,
      ),
      'kind' => array(
        'description' => 'Type of the bundle.',
        'type' => 'varchar',
        'length' => 8,
        'not null' => TRUE,
        'default' => 'css',
      ),
      'title' => array(
        'description' => 'Human readable title.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'size' => array(
        'description' => 'File size in bytes.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'enabled' => array(
        'description' => '1 if the bundle is enabled, 0 otherwise.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => TRUE,
      ),
      'minify' => array(
        'description' => 'Specified file minification.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => TRUE,
      ),
      'processed' => array(
        'description' => 'Path the bundle is currently stored at.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'processed_size' => array(
        'description' => 'Processed file\'s size in bytes',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'cc' => array(
        'description' => 'Conditional comment condition',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
    ),
    'indexes' => array(
      'kind' => array('kind'),
    ),
    'primary key' => array('bid'),
  );

  $schema['sf_cache_file'] = array(
    'description' => 'Stores information about each unprocessed file.',
    'fields' => array(
      'fid' => array(
        'description' => 'Unique file ID.',
        'type' => 'serial',
        'not null' => TRUE,
      ),
      'filepath' => array(
        'description' => 'The path the file is stored at.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'kind' => array(
        'description' => 'Type of the file.',
        'type' => 'varchar',
        'length' => 8,
        'not null' => TRUE,
        'default' => 'css',
      ),
      'bid' => array(
        'description' => 'ID of the bundle this file belongs to.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => -1,
      ),
      'size' => array(
        'description' => 'File size in bytes.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'hash' => array(
        'description' => 'A hash of the file for staleness checks.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
      'weight' => array(
        'description' => 'Used for ordering files in a bundle.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'enabled' => array(
        'description' => 'If 0, the file will never be added.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => TRUE,
      ),
      'minify' => array(
        'description' => 'Specified file minification.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => TRUE,
      ),
      'processed' => array(
        'description' => 'If standalone, processed filepath.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'processed_size' => array(
        'description' => 'Processed file\'s size in bytes.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'cc' => array(
        'description' => 'Conditional comment condition',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
    ),
    'indexes' => array(
      'kind' => array('kind'),
      'bid' => array('bid'),
    ),
    'unique keys' => array(
      'filepath' => array('filepath'),
    ),
    'primary key' => array('fid'),
  );

  $schema['sf_cache_file_statistics'] = array(
    'description' => 'Stores information about requested files.',
    'fields' => array(
      'access_sec' => array(
        'description' => 'The time the file is requested.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'access_msec' => array(
        'description' => 'The msec part of the time the file is requested.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'fid' => array(
        'description' => 'The ID of the added file.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'request' => array(
        'description' => 'The path on which the file is added.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'type' => array(
        'description' => 'Core, module, theme, ...',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
      'scope' => array(
        'description' => 'Header, footer, ...',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
    ),
    'indexes' => array(
      'fid' => array('fid'),
      'request' => array('request'),
    ),
    'primary key' => array('access_sec', 'access_msec', 'fid'),
  );

  $schema['sf_cache_file_relations'] = array(
    'description' => 'Stores information about how files are related.',
    'fields' => array(
      'fid' => array(
        'description' => 'The ID of the file.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'import_fid' => array(
        'description' => 'The ID of the file this file contains.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('fid', 'import_fid'),
  );

  $schema['sf_cache_temp_counter'] = array(
    'description' => 'Used as temporary table when doing automatic bundle generation.',
    'fields' => array(
      'fid' => array(
        'description' => '',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'filepath' => array(
        'description' => '',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'kind' => array(
        'description' => '',
        'type' => 'varchar',
        'length' => 8,
        'not null' => TRUE,
        'default' => 'css',
      ),
      'size' => array(
        'description' => '',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'total' => array(
        'description' => '',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('fid'),
  );

  $schema['sf_cache_temp_relation'] = array(
    'description' => 'Used as temporary storage when doing automatic bundle generation.',
    'fields' => array(
      'fid1' => array(
        'description' => '',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'fid2' => array(
        'description' => '',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'quantity' => array(
        'description' => '',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('fid1', 'fid2'),
  );

  return $schema;
}


/**
 * Implementation of hook_install().
 */
function sf_cache_install() {
  // Create tables.
  drupal_install_schema('sf_cache');

  // Make sure we run last so that the replacement function actually replaces
  // everything.
  db_query("UPDATE {system} SET weight = 250 WHERE name = 'sf_cache'");

  // Precreate a security hash for use on the GET composite file update.
  variable_set('sf_cache_security_hash', md5(microtime()));

  // Inform the user that the bundle replacement is not enabled by default.
  drupal_set_message(t('Bundle replacement is not enabled by default for Support File Cache module. Go to the <a href="@config-page">configuration page</a> to enable it.', array('@config-page' => url('admin/build/sf_cache/config'))));
}

/**
 * Implementation of hook_enable().
 */
function sf_cache_enable() {
  require_once drupal_get_path('module', 'sf_cache') .'/sf_cache.admin.inc';
  sf_cache_detect_files();
}

/**
 * Implementation of hook_uninstall().
 */
function sf_cache_uninstall() {
  // Remove tables.
  drupal_uninstall_schema('sf_cache');

  // Delete all variables.
  variable_del('sf_cache_autoupdate');
  variable_del('sf_cache_constant_config_hash');
  variable_del('sf_cache_css_server');
  variable_del('sf_cache_css_source');
  variable_del('sf_cache_css_target');
  variable_del('sf_cache_enabled');
  variable_del('sf_cache_excludes');
  variable_del('sf_cache_graphics_revision');
  variable_del('sf_cache_graphics_server');
  variable_del('sf_cache_js_server');
  variable_del('sf_cache_js_source');
  variable_del('sf_cache_js_target');
  variable_del('sf_cache_logging');
  variable_del('sf_cache_scopes');
  variable_del('sf_cache_security_hash');
  variable_del('sf_cache_use_inode');
  variable_del('sf_cache_writable');
  variable_del('sf_cache_minify_css_mode');
  variable_del('sf_cache_minify_css_executable');
  variable_del('sf_cache_minify_css_parameters');
  variable_del('sf_cache_minify_js_mode');
  variable_del('sf_cache_minify_js_executable');
  variable_del('sf_cache_minify_js_parameters');
}

function sf_cache_update_4() {
  $ret[] = update_sql("UPDATE {system} SET weight = 250 WHERE name = 'sf_cache'");
  return $ret;
}