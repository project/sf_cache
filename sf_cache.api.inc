<?php

/**
 * @file
 * Contains API functions for Support File Cache's entities.
 */


/**
 * Minification mode: Disabled.
 */
define('SF_CACHE_MINIFICATION_DISABLED', 0x0000);

/**
 * Minification mode: Use built-in minifier (only applicable for CSS).
 */
define('SF_CACHE_MINIFICATION_BUILTIN', 0x0001);

/**
 * Minification mode: Use a third party minification application.
 * Note: this constant is also defined in sf_cache.js.
 */
define('SF_CACHE_MINIFICATION_EXTERNAL', 0x0002);

/**
 * Minification mode: Use JSMin minifier (only applicable for JS).
 */
define('SF_CACHE_MINIFICATION_JSMIN', 0x0003);


/**
 * Graphic revision handling: Don't add a revision number to the URL.
 */
define('SF_CACHE_GRAPHICS_REV_NONE', 0x0000);

/**
 * Graphic revision handling: Add a revision number to the query.
 */
define('SF_CACHE_GRAPHICS_REV_QUERY', 0x0001);

/**
 * Graphic revision handling: Add a revision number before the file extension.
 */
define('SF_CACHE_GRAPHICS_REV_FILENAME', 0x0002);


/**
 * Returns information about a file.
 *
 * @param $path
 *   The metapath to the file information should be retrieved for.
 * @param $kind
 *   The type of the file.
 * @param $imports
 *   Whether to determine all CSS imports of the file.
 * @return
 *   An associative array containing the file size in bytes and a hash. FALSE
 *   if the stat can't be performed successfully on the file (e.g. it doesn't
 *   exist).
 */
function sf_cache_file_info($path, $kind, $imports = FALSE) {
  static $inode = NULL;

  if ($inode == NULL) {
    $inode = variable_get('sf_cache_use_inode', TRUE);
  }

  // Get the actual path of the file.
  $path = sf_cache_file_path($path, $kind);

  // Try to retrieve information about the file using stat.
  if (file_exists($path) && $stat = stat($path)) {
    $return = array(
      'size' => $stat['size'],
      'hash' => md5($path .':'. ($inode ? $stat['mtime'] .':'. $stat['ino'] .':' : '') . $stat['size']),
    );

    // Optionally determine the imported CSS files.
    if ($kind == 'css' && $imports == TRUE) {
      $return['imports'] = _sf_cache_determine_css_imports($path);
    }

    return $return;
  }
  else {
    return FALSE;
  }
}

/**
 * Recursively determines all CSS files a given file imports.
 *
 * @param $file
 *   The file for which the imported CSS files are to be determined.
 * @return
 *   An array with all paths this CSS file imports.
 */
function _sf_cache_determine_css_imports($file) {
  $imports = array();

  $matches = array();
  if (preg_match_all('/@import\s*(?:url\()?[\'"]?(?![a-z]+:)([^\'"\()]+)[\'"]?\)?;/', file_get_contents($file), $matches)) {
    // Loop over all found @import statements.
    foreach ($matches[1] as $import_file) {
      // Only insert a slash between the directory and the new file and normalize it.
      $import_file = _sf_cache_normalize_path(dirname($file) . (dirname($file) ? '/' : '') . $import_file);

      if (!isset($imports[$import_file]) && file_exists($import_file)) {
        $imports[$import_file] = TRUE;
        $imports += array_flip(_sf_cache_determine_css_imports($import_file));
      }
    }
  }

  return array_keys($imports);
}

/**
 * Normalizes a path by removing folder/.. segments from it.
 *
 * @param $path
 *   The path to normalize.
 * @return
 *   The normalized path.
 */
function _sf_cache_normalize_path($path) {
  $last = '';
  // Remove folder/../ segments where possible.
  while ($path != $last) {
    $last = $path;
    $path = preg_replace('`(^|/)(?!\.\./)([^/]+)/\.\./`', '$1', $path);
  }

  return $path;
}


/**
 * Returns the actual path of a file. Used directly before accessing the file.
 *
 * @param $path
 *   The meta path to the file.
 * @param $kind
 *   The type of the file.
 * @return
 *   The actual path to the file, including any base directories.
 */
function sf_cache_file_path($path, $kind) {
  static $_prefixes = NULL;

  if ($_prefixes == NULL) {
    $_prefixes = array(
      'css' => rtrim(variable_get('sf_cache_css_source', ''), '/'),
      'js'  => rtrim(variable_get('sf_cache_js_source', ''), '/'),
    );

    if ($_prefixes['css'] == '.') {
      $_prefixes['css'] = '';
    }

    if ($_prefixes['js'] == '.') {
      $_prefixes['js'] = '';
    }
  }

  if (empty($_prefixes[$kind])) {
    return $path;
  }
  else {
    return $_prefixes[$kind] .'/'. $path;
  }
}

/**
 * Returns a unique configuration hash.
 *
 * This function is used to generate a hash that is unique to the current
 * configuration of Support File Cache. It is used to ensure that new unique
 * file names are created when something about the configuration changes.
 *
 * @param $flush
 *   (optional) If set to TRUE, the static variable cache is flushed. This
 *   should be done after the configuration has been altered.
 * @return
 *   A 128 bit hex hash unique for the current configuration.
 */
function sf_cache_config_hash($flush = FALSE) {
  static $hash = NULL;

  if ($hash == NULL || $flush) {
    // Note: this config hash can't be set using a UI. It can be used to
    // prevent constant updates when working in a multi-user environment
    // where every user has different host names.
    $hash = variable_get('sf_cache_constant_config_hash', '');

    if (empty($hash)) {
      $hash =
        variable_get('sf_cache_use_inode', TRUE) .':'.
        variable_get('sf_cache_graphics_server', '') .':'.
        variable_get('sf_cache_graphics_revision', SF_CACHE_GRAPHICS_REV_NONE) .':'.
        variable_get('sf_cache_css_source', '') .':'.
        variable_get('sf_cache_css_target', 'sf_css') .':'.
        variable_get('sf_cache_css_server', '') .':'.
        variable_get('sf_cache_js_source', '') .':'.
        variable_get('sf_cache_js_target', 'sf_js') .':'.
        variable_get('sf_cache_js_server', '') .':'.
        variable_get('sf_cache_minify_css_mode', SF_CACHE_MINIFICATION_BUILTIN) .':'.
        variable_get('sf_cache_minify_css_executable', '') .':'.
        variable_get('sf_cache_minify_css_parameters', '') .':'.
        variable_get('sf_cache_minify_js_mode', SF_CACHE_MINIFICATION_DISABLED) .':'.
        variable_get('sf_cache_minify_js_executable', '') .':'.
        variable_get('sf_cache_minify_js_parameters', '');
    }

    $hash = md5($hash);
  }

  return $hash;
}

/**
 * Create a database record for a new file.
 *
 * @param $kind
 *   The type of the file.
 * @param $path
 *   The meta path of the file.
 * @param $clear
 *   (optional) A boolean indicating whether the cache should be cleared after
 *   the new file record has been created.
 *
 * @return
 *   Returns the ID of the new file if successfully created.
 */
function sf_cache_file_create($kind, $path, $clear = TRUE) {
  if (!sf_cache_exclude_check($path) && $info = sf_cache_file_info($path, $kind)) {
    $file = array(
      'fid' => -1,
      'filepath' => $path,
      'kind' => $kind,
      'bid' => -1,
      'size' => $info['size'],
      'hash' => $info['hash'],
      'weight' => 0,
      'enabled' => 1,
      'minify' => 1,
      'processed' => '',
      'processed_size' => 0,
    );

    // Create the new database record and fetch the new file ID.
    $result = db_query("INSERT INTO {sf_cache_file} (filepath, kind, size, hash) VALUES ('%s', '%s', %d, '%s')", $file['filepath'], $file['kind'], $file['size'], $file['hash']);
    $file['fid'] = db_last_insert_id('sf_cache_file', 'fid');

    if ($clear) {
      sf_cache_clear_cache();
    }

    module_invoke_all('sfc_file_create', $file);

    return $file;
  }
  else {
    // TODO: watchdog: file not found
  }
}

/**
 * Update the database record for a file.
 *
 * @param $file
 *   An associative array with information about the file. It needs to have
 *   the index 'fid' set so that the according file can be updated.
 * @param $info
 *   (optional) A file information array, as returned by sf_cache_file_info().
 * @param $update
 *   (optional) If TRUE (default), bundle updates will be performed after the
 *   file has been saved. Pass FALSE to suppress this behavior.
 * @return
 *   Returns the file array or FALSE if the file is invalid. The file array
 *   contains the key '#updated' which indicates whether there have been changes
 *   to the file.
 */
function sf_cache_file_save($file, $info = NULL, $update = TRUE) {
  // We need a file ID set to be able to update.
  if (!isset($file['fid'])) {
    return FALSE;
  }

  if (empty($info)) {
    // Fetch new information about the file.
    $info = sf_cache_file_info($file['filepath'], $file['kind']);
    if (!$info) {
      $info = array('hash' => '');
    }
  }

  $file = array_merge($file, $info);
  $old_file = sf_cache_file_load($file['fid']);
  $keys = array('bid' => "%d", 'hash' => "'%s'", 'size' => "%d", 'weight' => "%d", 'enabled' => "%d", 'minify' => "%d", 'processed' => "'%s'", 'processed_size' => "%d", 'cc' => "'%s'");
  $query = array();
  $parameters = array();

  if (empty($file['bid'])) {
    $file['bid'] = SF_CACHE_UNBUNDLED;
  }
  if (!empty($file['processed'])) {
    $file['processed_size'] = @filesize($file['processed']);
  }

  // Only add update SQL code if there are actual changes.
  foreach ($keys as $key => $placeholder) {
    if (isset($file[$key]) && $file[$key] != $old_file[$key]) {
      $query[] = $key .' = '. $placeholder;
      $parameters[] = $file[$key];
    }
  }

  if (!empty($query)) {
    $query = "UPDATE {sf_cache_file} SET ". implode(', ', $query) ." WHERE fid = %d";
    $parameters[] = $file['fid'];
    db_query($query, $parameters);

    sf_cache_clear_cache();

    // Add missing keys to the file array and remove superfluous keys.
    $file = array_intersect_key($file, $old_file) + $old_file;

    // Remove the old file.
    if (!empty($old_file['processed']) && $old_file['processed'] != $file['processed']) {
      module_invoke_all('sfc_composite_retract', $file, $old_file['processed']);

      // Delete the actual file.
      file_delete($old_file['processed']);
    }

    module_invoke_all('sfc_file_update', $file, $old_file);

    // Update all files.
    if ($update) {
      sf_cache_update_composites();
    }

    $file['#updated'] = TRUE;
  }
  else {
    $file['#updated'] = FALSE;
  }

  return $file;
}

/**
 * Load files from database.
 *
 * @param $key
 *   (optional) If numeric, the file array for that file ID is returned,
 *   if it's a string, the files array returned will be keyed to this key.
 *   Valid keys are 'fid', 'filepath', 'hash' and 'processed'.
 * @param $flush
 *   (optional) If TRUE, the results of the static variable cache are cleared.
 * @param $return
 *   (optional) If set to FALSE, no data is returned (used for only clearing
 *   the static var cache without actually retrieving data).
 * @return
 *   Depending on the parameters, it will return an array of files, grouped by
 *   kind, keyed to the key, if it is a string. If the key has been numeric,
 *   the file array will be returned. If it does not exist, FALSE will be returned.
 */
function sf_cache_file_load($key = 'fid', $flush = FALSE, $return = TRUE) {
  static $files;

  if (!isset($files) || $flush) {
    $files = array();
  }

  if ($return) {
    // If the first parameter is an integer, we return only that file instead of all files.
    if (is_numeric($key)) {
      $fid = $key;
      $key = 'fid';
    }

    if (!isset($files[$key])) {
      $result = db_query("SELECT fid, filepath, kind, bid, size, hash, weight, enabled, minify, processed, processed_size, cc FROM {sf_cache_file}");

      $files[$key] = array('css' => array(), 'js' => array());
      // Load the data into an array.
      while ($file = db_fetch_array($result)) {
        if (!isset($file['bid'])) {
          $file['bid'] = SF_CACHE_UNBUNDLED;
        }
        $files[$key][$file['kind']][$file[$key]] = $file;
      }
    }

    if (isset($fid)) {
      if (isset($files[$key]['css'][$fid])) {
        return $files[$key]['css'][$fid];
      }
      elseif (isset($files[$key]['js'][$fid])) {
        return $files[$key]['js'][$fid];
      }
      else {
        return FALSE;
      }
    }
    else {
      return $files[$key];
    }
  }
}

/**
 * Deletes the database record for a file.
 *
 * @param $fid
 *   The file ID or a file array of the file that should be removed from the
 *   database.
 * @param $update
 *   (optional) If TRUE (default), bundle updates will be performed after the
 *   file has been deleted. Pass FALSE to suppress this behavior.
 * @return
 *   Returns the file array if it has been found/deleted, FALSE otherwise.
 */
function sf_cache_file_delete($fid, $update = TRUE) {
  $file = sf_cache_file_load(is_array($fid) ? $fid['fid'] : $fid);

  if ($file) {
    db_query("DELETE FROM {sf_cache_file} WHERE fid = %d", $fid);
    db_query("DELETE FROM {sf_cache_file_statistics} WHERE fid = %d", $file['fid']);

    // Remove the physical processed file.
    if (!empty($file['processed'])) {
      module_invoke_all('sfc_composite_retract', $file, $file['processed']);

      // Delete the actual file.
      file_delete($file['processed']);
    }

    module_invoke_all('sfc_file_delete', $file);
  }

  if ($update) {
    // Update all files accordingly.
    sf_cache_update_composites();
  }

  return $file ? $file : FALSE;
}

/**
 * Deletes all files without corresponding real file.
 *
 * @param $update
 *  (optional) If TRUE (default), bundle updates will be performed after the
 *  orphaned file have been deleted. Pass FALSE to suppress this behavior.
 */
function sf_cache_file_delete_orphaned($update = TRUE) {
  $orphaned = db_query("SELECT fid, kind, filepath FROM {sf_cache_file} WHERE hash = ''");
  $deleted = array();

  while ($file = db_fetch_array($orphaned)) {
    if (!file_exists(sf_cache_file_path($file['filepath'], $file['kind']))) {
      $file = sf_cache_file_delete($file['fid'], FALSE);
      if ($file) {
        $deleted[] = check_plain($file['filepath']);
      }
    }
  }

  if ($update) {
    // Update all files accordingly.
    sf_cache_update_composites();
  }

  return $deleted;
}

/**
 * Creates a database record for a new bundle.
 *
 * @param $kind
 *   The type of the bundle.
 * @param $title
 *   The name of the bundle.
 * @param $clear
 *   (optional) A boolean indicating whether the cache should be cleared after
 *   the new file record has been created.
 * @return
 *   The newly created bundle.
 */
function sf_cache_bundle_create($bundle, $clear = TRUE) {
  $bundle += array(
    'title' => 'Bundle',
    'minify' => 1,
  );
  db_query("INSERT INTO {sf_cache_bundle} (kind, title, minify) VALUES ('%s', '%s', %d)", $bundle['kind'], $bundle['title'], $bundle['minify']);

  // Fetch the new bundle's ID.
  $bid = db_last_insert_id('sf_cache_bundle', 'bid');

  if ($clear) {
    sf_cache_clear_cache();
  }

  // Invoke the bundle creation hooks.
  $bundle = sf_cache_bundle_load($bid, TRUE);
  module_invoke_all('sfc_bundle_create', $bundle);

  return $bundle;
}

/**
 * Updates the database record for a bundle.
 *
 * @param $bundle
 *   The new bundle. Has to include the bundle ID.
 * @param $update
 *   (optional) If TRUE (default), bundle updates will be performed after the
 *   bundle has been saved. Pass FALSE to suppress this behavior.
 * @param $clear
 *   (optional) A boolean indicating whether the cache should be cleared after
 *   the new bundle record has been saved.
 * @return
 *   Returns the bundle array or FALSE if the bundle is invalid. The bundle array
 *   contains the key '#updated' which indicates whether there have been changes
 *   to the bundle.
 */
function sf_cache_bundle_save($bundle, $update = TRUE, $clear = TRUE) {
  // We need a bundle ID set to be able to update.
  if (!isset($bundle['bid'])) {
    return FALSE;
  }

  $old_bundle = sf_cache_bundle_load($bundle['bid']);
  $keys = array('title' => "'%s'", 'size' => "%d", 'minify' => "%d", 'processed' => "'%s'", 'processed_size' => "%d", 'enabled' => "%d", 'cc' => "'%s'");
  $query = array();
  $parameters = array();

  if (!empty($bundle['processed'])) {
    $bundle['processed_size'] = @filesize($bundle['processed']);
  }

  // Only add update SQL code if there are actual changes.
  foreach ($keys as $key => $placeholder) {
    if (isset($bundle[$key]) && $bundle[$key] != $old_bundle[$key]) {
      $query[] = $key .' = '. $placeholder;
      $parameters[] = $bundle[$key];
    }
  }

  if (!empty($query)) {
    $query = "UPDATE {sf_cache_bundle} SET ". implode(', ', $query) ." WHERE bid = %d";
    $parameters[] = $bundle['bid'];
    db_query($query, $parameters);

    // Add missing keys to the bundle array and remove superfluous keys.
    $bundle = array_intersect_key($bundle, $old_bundle) + $old_bundle;

    // TODO: Rename the bundle if title has changed.

    // Remove an old bundle
    if (!empty($old_bundle['processed']) && $old_bundle['processed'] != $bundle['processed']) {
      module_invoke_all('sfc_composite_retract', $bundle, $old_bundle['processed']);

      // Delete the actual file.
      file_delete($old_bundle['processed']);
    }

    if ($clear) {
      sf_cache_clear_cache();
    }

    // Invoke the bundle update hooks.
    module_invoke_all('sfc_bundle_update', $bundle, $old_bundle);

    // Update all files.
    if ($update) {
      sf_cache_update_composites();
    }

    $bundle['#updated'] = TRUE;
  }
  else {
    $bundle['#updated'] = FALSE;
  }

  return $bundle;
}

/**
 * Loads the database record for a bundle.
 *
 * @param $bid
 *   (optional) The ID of the bundle to be loaded. If not set, all bundles
 *   will be returned.
 * @param $flush
 *   (optional) If TRUE, the results of the static variable cache are cleared.
 * @param $return
 *   (optional) If set to FALSE, no data is returned (used for only clearing
 *   the static var cache without actually retrieving data).
 * @return
 *   Depending on the parameters, it will return an array of bundles, grouped by
 *   kind, if no bundle ID was specified. If the $bid parameter has been numeric,
 *   the bundle is returned. If it does not exist, FALSE will be returned.
 */
function sf_cache_bundle_load($bid = NULL, $flush = FALSE, $return = TRUE) {
  static $bundles;

  if ($flush) {
    $bundles = NULL;
  }

  if ($return) {
    if (empty($bundles) || $flush) {
      $result = db_query("SELECT bid, kind, title, size, processed, processed_size, enabled, minify, cc FROM {sf_cache_bundle}");

      $files = array('css' => array(), 'js' => array());
      $bundles = array();
      // Load the data into an array.
      while ($bundle = db_fetch_array($result)) {
        $bundles[$bundle['kind']][$bundle['bid']] = $bundle;
      }
    }

    if (isset($bid)) {
      if (isset($bundles['css'][$bid])) {
        return $bundles['css'][$bid];
      }
      else if (isset($bundles['js'][$bid])) {
        return $bundles['js'][$bid];
      }
      else {
        return FALSE;
      }
    }
    else {
      return $bundles;
    }
  }
}

/**
 * Deletes the database record for a bundle.
 *
 * @param $bid
 *   The bundle ID or a bundle array of the bundle that should be removed from
 *   the database.
 * @param $clear
 *   (optional) A boolean indicating whether the cache should be cleared after
 *   the bundle has been deleted.
 * @return
 *   Returns the file array if it has been found/deleted, FALSE otherwise.
 */
function sf_cache_bundle_delete($bid, $clear = TRUE) {
  $bundle = sf_cache_bundle_load(is_array($bid) ? $bid['bid'] : $bid);

  if ($bundle) {
    db_query("DELETE FROM {sf_cache_bundle} WHERE bid = %d", $bundle['bid']);
    db_query("UPDATE {sf_cache_file} SET bid = -1 WHERE bid = %d", $bundle['bid']);

    // Remove the physical processed file.
    if (!empty($bundle['processed'])) {
      module_invoke_all('sfc_composite_retract', $bundle, $bundle['processed']);

      // Delete the actual file.
      file_delete($bundle['processed']);
    }

    if ($clear) {
      sf_cache_clear_cache();
    }

    module_invoke_all('sfc_bundle_delete', $bundle);
  }

  return $bundle ? $bundle : FALSE;
}

/**
 * Deletes all bundles without any associated files.
 *
 * @return
 *   An array of removed bundles.
 */
function sf_cache_bundle_delete_empty() {
  $result = db_query("SELECT b.bid, COUNT(f.fid) as num FROM {sf_cache_bundle} b LEFT JOIN {sf_cache_file} f ON b.bid = f.bid GROUP BY b.bid HAVING num = 0");
  $deleted = array();

  while ($bundle = db_fetch_array($result)) {
    $bundle = sf_cache_bundle_delete($bundle['bid']);
    if ($bundle) {
      $deleted[] = check_plain($bundle['title']);
    }
  }

  return $deleted;
}

/**
 * Adds a path to the list of excluded paths. Newly excluded files will be deleted.
 *
 * @param $path
 *   The meta path that is to be excluded.
 * @return
 *   If the path was not already on the list, an array of newly excluded (and
 *   now deleted) files is returned, otherwise the return value is FALSE.
 */
function sf_cache_exclude_add($path) {
  $excludes = variable_get('sf_cache_excludes', array());

  if (!in_array($path, $excludes)) {
    $excludes[] = $path;
    sort($excludes);
    variable_set('sf_cache_excludes', $excludes);

    // FLush the exclude check's static var cache.
    sf_cache_exclude_check(NULL, TRUE);

    $affected = array();
    $files = sf_cache_file_catalog();
    foreach ($files as $filepath => $file) {
      if (sf_cache_exclude_check($filepath)) {
        sf_cache_file_delete($file['fid'], FALSE);
        $affected[] = $filepath;
      }
    }

    sf_cache_update_composites();

    return $affected;
  }
  else {
    return FALSE;
  }
}

/**
 * Removes a path to the list of excluded paths.
 *
 * @param $path
 *   The meta path that is to be removed from exclusion.
 * @param $update
 *   (optional) If TRUE (default), Support File Cache looks for new files and
 *   will perform bundle updates after the path has been removed from the list
 *   of excludes. Pass FALSE to suppress this behavior.
 * @return
 *   If the path was not on the list, FALSE is returned; TRUE otherwise.
 */
function sf_cache_exclude_remove($path, $update = TRUE) {
  $excludes = variable_get('sf_cache_excludes', array());

  if (($pos = array_search($path, $excludes)) !== FALSE) {
    unset($excludes[$pos]);
    variable_set('sf_cache_excludes', $excludes);

    if ($update) {
      sf_cache_detect_files();
      sf_cache_update_composites();
    }

    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
 * Checks whether a path or file is excluded.
 *
 * @param $path
 *   (optional) The meta path that should be checked for exclusion status.
 * @param $flush
 *   (optional) If TRUE, the static variable cache will be cleared.
 * @return
 *   Returns TRUE when the path is excluded; FALSE otherwise.
 */
function sf_cache_exclude_check($path = NULL, $flush = FALSE) {
  static $excludes = NULL;

  if ($excludes == NULL || $flush) {
    $excludes = variable_get('sf_cache_excludes', array());
  }

  if ($path != NULL) {
    foreach ($excludes as $exclude) {
      if (strpos($path, $exclude) === 0) {
        return TRUE;
      }
    }
  }

  return FALSE;
}
